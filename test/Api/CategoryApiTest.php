<?php
/**
 * CategoryApiTest
 * PHP version 5
 *
 * @category Class
 * @package  PlanningPme
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * PlanningPME.API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v4-7-0-27
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.64
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the endpoint.
 */

namespace PlanningPme;

use PlanningPme\Configuration;
use PlanningPme\ApiException;
use PlanningPme\ObjectSerializer;
use PHPUnit\Framework\TestCase;

/**
 * CategoryApiTest Class Doc Comment
 *
 * @category Class
 * @package  PlanningPme
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class CategoryApiTest extends TestCase
{

    /**
     * Setup before running any test cases
     */
    public static function setUpBeforeClass(): void
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp(): void
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown(): void
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass(): void
    {
    }

    /**
     * Test case for categoryDelete
     *
     * .
     *
     */
    public function testCategoryDelete()
    {
    }

    /**
     * Test case for categoryGet
     *
     * .
     *
     */
    public function testCategoryGet()
    {
    }

    /**
     * Test case for categoryGet_0
     *
     * .
     *
     */
    public function testCategoryGet0()
    {
    }

    /**
     * Test case for categoryPost
     *
     * .
     *
     */
    public function testCategoryPost()
    {
    }

    /**
     * Test case for categoryPut
     *
     * .
     *
     */
    public function testCategoryPut()
    {
    }
}
