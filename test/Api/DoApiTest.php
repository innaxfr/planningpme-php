<?php
/**
 * DoApiTest
 * PHP version 5
 *
 * @category Class
 * @package  PlanningPme
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * PlanningPME.API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v4-7-0-27
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.64
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the endpoint.
 */

namespace PlanningPme;

use PlanningPme\Configuration;
use PlanningPme\ApiException;
use PlanningPme\ObjectSerializer;
use PHPUnit\Framework\TestCase;

/**
 * DoApiTest Class Doc Comment
 *
 * @category Class
 * @package  PlanningPme
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class DoApiTest extends TestCase
{

    /**
     * Setup before running any test cases
     */
    public static function setUpBeforeClass(): void
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp(): void
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown(): void
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass(): void
    {
    }

    /**
     * Test case for doAlert
     *
     * .
     *
     */
    public function testDoAlert()
    {
    }

    /**
     * Test case for doChangeBlock
     *
     * .
     *
     */
    public function testDoChangeBlock()
    {
    }

    /**
     * Test case for doDelete
     *
     * .
     *
     */
    public function testDoDelete()
    {
    }

    /**
     * Test case for doDissociate
     *
     * .
     *
     */
    public function testDoDissociate()
    {
    }

    /**
     * Test case for doEvents
     *
     * .
     *
     */
    public function testDoEvents()
    {
    }

    /**
     * Test case for doGet
     *
     * .
     *
     */
    public function testDoGet()
    {
    }

    /**
     * Test case for doGetDoList
     *
     * .
     *
     */
    public function testDoGetDoList()
    {
    }

    /**
     * Test case for doGetDoPrint
     *
     * .
     *
     */
    public function testDoGetDoPrint()
    {
    }

    /**
     * Test case for doPost
     *
     * .
     *
     */
    public function testDoPost()
    {
    }

    /**
     * Test case for doPut
     *
     * .
     *
     */
    public function testDoPut()
    {
    }

    /**
     * Test case for doRecurrenceDesc
     *
     * .
     *
     */
    public function testDoRecurrenceDesc()
    {
    }

    /**
     * Test case for doSearch
     *
     * .
     *
     */
    public function testDoSearch()
    {
    }

    /**
     * Test case for doSpan
     *
     * .
     *
     */
    public function testDoSpan()
    {
    }

    /**
     * Test case for doUnperiodize
     *
     * .
     *
     */
    public function testDoUnperiodize()
    {
    }
}
