<?php
/**
 * PlanningPMEAPIModelsExportModelTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  PlanningPme
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * PlanningPME.API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v4-7-0-27
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.64
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace PlanningPme;

use PHPUnit\Framework\TestCase;

/**
 * PlanningPMEAPIModelsExportModelTest Class Doc Comment
 *
 * @category    Class
 * @description PlanningPMEAPIModelsExportModel
 * @package     PlanningPme
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class PlanningPMEAPIModelsExportModelTest extends TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass(): void
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp(): void
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown(): void
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass(): void
    {
    }

    /**
     * Test "PlanningPMEAPIModelsExportModel"
     */
    public function testPlanningPMEAPIModelsExportModel()
    {
    }

    /**
     * Test attribute "assignments"
     */
    public function testPropertyAssignments()
    {
    }

    /**
     * Test attribute "by_day"
     */
    public function testPropertyByDay()
    {
    }

    /**
     * Test attribute "by_department"
     */
    public function testPropertyByDepartment()
    {
    }

    /**
     * Test attribute "categories"
     */
    public function testPropertyCategories()
    {
    }

    /**
     * Test attribute "customers"
     */
    public function testPropertyCustomers()
    {
    }

    /**
     * Test attribute "date_from"
     */
    public function testPropertyDateFrom()
    {
    }

    /**
     * Test attribute "date_to"
     */
    public function testPropertyDateTo()
    {
    }

    /**
     * Test attribute "departments"
     */
    public function testPropertyDepartments()
    {
    }

    /**
     * Test attribute "filters"
     */
    public function testPropertyFilters()
    {
    }

    /**
     * Test attribute "projects"
     */
    public function testPropertyProjects()
    {
    }

    /**
     * Test attribute "resources"
     */
    public function testPropertyResources()
    {
    }

    /**
     * Test attribute "skills"
     */
    public function testPropertySkills()
    {
    }

    /**
     * Test attribute "sub_projects"
     */
    public function testPropertySubProjects()
    {
    }

    /**
     * Test attribute "tasks"
     */
    public function testPropertyTasks()
    {
    }

    /**
     * Test attribute "task_statutes"
     */
    public function testPropertyTaskStatutes()
    {
    }

    /**
     * Test attribute "unavailabilities"
     */
    public function testPropertyUnavailabilities()
    {
    }

    /**
     * Test attribute "unavailability_statutes"
     */
    public function testPropertyUnavailabilityStatutes()
    {
    }
}
