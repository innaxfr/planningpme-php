<?php
/**
 * PlanningPMEAPIModelsEffConditionModelTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  PlanningPme
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * PlanningPME.API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v4-7-0-27
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.64
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace PlanningPme;

use PHPUnit\Framework\TestCase;

/**
 * PlanningPMEAPIModelsEffConditionModelTest Class Doc Comment
 *
 * @category    Class
 * @description PlanningPMEAPIModelsEffConditionModel
 * @package     PlanningPme
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class PlanningPMEAPIModelsEffConditionModelTest extends TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass(): void
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp(): void
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown(): void
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass(): void
    {
    }

    /**
     * Test "PlanningPMEAPIModelsEffConditionModel"
     */
    public function testPlanningPMEAPIModelsEffConditionModel()
    {
    }

    /**
     * Test attribute "id"
     */
    public function testPropertyId()
    {
    }

    /**
     * Test attribute "order"
     */
    public function testPropertyOrder()
    {
    }

    /**
     * Test attribute "sources"
     */
    public function testPropertySources()
    {
    }
}
