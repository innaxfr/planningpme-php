<?php
/**
 * PlanningPMEAPIModelsCustomerModel
 *
 * PHP version 5
 *
 * @category Class
 * @package  PlanningPme
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * PlanningPME.API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v4-7-0-27
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.64
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace PlanningPme\Model;

use \ArrayAccess;
use \PlanningPme\ObjectSerializer;

/**
 * PlanningPMEAPIModelsCustomerModel Class Doc Comment
 *
 * @category Class
 * @package  PlanningPme
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class PlanningPMEAPIModelsCustomerModel implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $swaggerModelName = 'PlanningPME.API.Models.CustomerModel';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = [
        'address' => 'string',
        'building' => 'string',
        'city' => 'string',
        'company' => 'string',
        'country' => 'string',
        'door_code' => 'string',
        'email' => 'string',
        'fax' => 'string',
        'fields' => 'map[string,object]',
        'first_name' => 'string',
        'floor' => 'string',
        'language' => 'string',
        'last_name' => 'string',
        'mobile' => 'string',
        'not_valid' => 'bool',
        'number' => 'string',
        'phone' => 'string',
        'state' => 'string',
        'style' => '\PlanningPme\Model\PlanningPMEAPIModelsStyleModel',
        'title' => 'string',
        'type' => 'string',
        'web_site' => 'string',
        'zip' => 'string'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerFormats = [
        'address' => null,
        'building' => null,
        'city' => null,
        'company' => null,
        'country' => null,
        'door_code' => null,
        'email' => null,
        'fax' => null,
        'fields' => null,
        'first_name' => null,
        'floor' => null,
        'language' => null,
        'last_name' => null,
        'mobile' => null,
        'not_valid' => null,
        'number' => null,
        'phone' => null,
        'state' => null,
        'style' => null,
        'title' => null,
        'type' => null,
        'web_site' => null,
        'zip' => null
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'address' => 'address',
        'building' => 'building',
        'city' => 'city',
        'company' => 'company',
        'country' => 'country',
        'door_code' => 'doorCode',
        'email' => 'email',
        'fax' => 'fax',
        'fields' => 'fields',
        'first_name' => 'firstName',
        'floor' => 'floor',
        'language' => 'language',
        'last_name' => 'lastName',
        'mobile' => 'mobile',
        'not_valid' => 'notValid',
        'number' => 'number',
        'phone' => 'phone',
        'state' => 'state',
        'style' => 'style',
        'title' => 'title',
        'type' => 'type',
        'web_site' => 'webSite',
        'zip' => 'zip'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'address' => 'setAddress',
        'building' => 'setBuilding',
        'city' => 'setCity',
        'company' => 'setCompany',
        'country' => 'setCountry',
        'door_code' => 'setDoorCode',
        'email' => 'setEmail',
        'fax' => 'setFax',
        'fields' => 'setFields',
        'first_name' => 'setFirstName',
        'floor' => 'setFloor',
        'language' => 'setLanguage',
        'last_name' => 'setLastName',
        'mobile' => 'setMobile',
        'not_valid' => 'setNotValid',
        'number' => 'setNumber',
        'phone' => 'setPhone',
        'state' => 'setState',
        'style' => 'setStyle',
        'title' => 'setTitle',
        'type' => 'setType',
        'web_site' => 'setWebSite',
        'zip' => 'setZip'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'address' => 'getAddress',
        'building' => 'getBuilding',
        'city' => 'getCity',
        'company' => 'getCompany',
        'country' => 'getCountry',
        'door_code' => 'getDoorCode',
        'email' => 'getEmail',
        'fax' => 'getFax',
        'fields' => 'getFields',
        'first_name' => 'getFirstName',
        'floor' => 'getFloor',
        'language' => 'getLanguage',
        'last_name' => 'getLastName',
        'mobile' => 'getMobile',
        'not_valid' => 'getNotValid',
        'number' => 'getNumber',
        'phone' => 'getPhone',
        'state' => 'getState',
        'style' => 'getStyle',
        'title' => 'getTitle',
        'type' => 'getType',
        'web_site' => 'getWebSite',
        'zip' => 'getZip'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$swaggerModelName;
    }

    const TITLE_MISS = 'Miss';
    const TITLE_MR = 'Mr';
    const TITLE_MS = 'Ms';
    const TYPE_COMPANY = 'Company';
    const TYPE_INDIVIDUAL = 'Individual';

    /**
     * Gets allowable values of the enum
     *
     * @return string[]
     */
    public function getTitleAllowableValues()
    {
        return [
            self::TITLE_MISS,
            self::TITLE_MR,
            self::TITLE_MS,
        ];
    }
    /**
     * Gets allowable values of the enum
     *
     * @return string[]
     */
    public function getTypeAllowableValues()
    {
        return [
            self::TYPE_COMPANY,
            self::TYPE_INDIVIDUAL,
        ];
    }

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['address'] = isset($data['address']) ? $data['address'] : null;
        $this->container['building'] = isset($data['building']) ? $data['building'] : null;
        $this->container['city'] = isset($data['city']) ? $data['city'] : null;
        $this->container['company'] = isset($data['company']) ? $data['company'] : null;
        $this->container['country'] = isset($data['country']) ? $data['country'] : null;
        $this->container['door_code'] = isset($data['door_code']) ? $data['door_code'] : null;
        $this->container['email'] = isset($data['email']) ? $data['email'] : null;
        $this->container['fax'] = isset($data['fax']) ? $data['fax'] : null;
        $this->container['fields'] = isset($data['fields']) ? $data['fields'] : null;
        $this->container['first_name'] = isset($data['first_name']) ? $data['first_name'] : null;
        $this->container['floor'] = isset($data['floor']) ? $data['floor'] : null;
        $this->container['language'] = isset($data['language']) ? $data['language'] : null;
        $this->container['last_name'] = isset($data['last_name']) ? $data['last_name'] : null;
        $this->container['mobile'] = isset($data['mobile']) ? $data['mobile'] : null;
        $this->container['not_valid'] = isset($data['not_valid']) ? $data['not_valid'] : null;
        $this->container['number'] = isset($data['number']) ? $data['number'] : null;
        $this->container['phone'] = isset($data['phone']) ? $data['phone'] : null;
        $this->container['state'] = isset($data['state']) ? $data['state'] : null;
        $this->container['style'] = isset($data['style']) ? $data['style'] : null;
        $this->container['title'] = isset($data['title']) ? $data['title'] : null;
        $this->container['type'] = isset($data['type']) ? $data['type'] : null;
        $this->container['web_site'] = isset($data['web_site']) ? $data['web_site'] : null;
        $this->container['zip'] = isset($data['zip']) ? $data['zip'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        $allowedValues = $this->getTitleAllowableValues();
        if (!is_null($this->container['title']) && !in_array($this->container['title'], $allowedValues, true)) {
            $invalidProperties[] = sprintf(
                "invalid value for 'title', must be one of '%s'",
                implode("', '", $allowedValues)
            );
        }

        $allowedValues = $this->getTypeAllowableValues();
        if (!is_null($this->container['type']) && !in_array($this->container['type'], $allowedValues, true)) {
            $invalidProperties[] = sprintf(
                "invalid value for 'type', must be one of '%s'",
                implode("', '", $allowedValues)
            );
        }

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->container['address'];
    }

    /**
     * Sets address
     *
     * @param string $address address
     *
     * @return $this
     */
    public function setAddress($address)
    {
        $this->container['address'] = $address;

        return $this;
    }

    /**
     * Gets building
     *
     * @return string
     */
    public function getBuilding()
    {
        return $this->container['building'];
    }

    /**
     * Sets building
     *
     * @param string $building building
     *
     * @return $this
     */
    public function setBuilding($building)
    {
        $this->container['building'] = $building;

        return $this;
    }

    /**
     * Gets city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->container['city'];
    }

    /**
     * Sets city
     *
     * @param string $city city
     *
     * @return $this
     */
    public function setCity($city)
    {
        $this->container['city'] = $city;

        return $this;
    }

    /**
     * Gets company
     *
     * @return string
     */
    public function getCompany()
    {
        return $this->container['company'];
    }

    /**
     * Sets company
     *
     * @param string $company company
     *
     * @return $this
     */
    public function setCompany($company)
    {
        $this->container['company'] = $company;

        return $this;
    }

    /**
     * Gets country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->container['country'];
    }

    /**
     * Sets country
     *
     * @param string $country country
     *
     * @return $this
     */
    public function setCountry($country)
    {
        $this->container['country'] = $country;

        return $this;
    }

    /**
     * Gets door_code
     *
     * @return string
     */
    public function getDoorCode()
    {
        return $this->container['door_code'];
    }

    /**
     * Sets door_code
     *
     * @param string $door_code door_code
     *
     * @return $this
     */
    public function setDoorCode($door_code)
    {
        $this->container['door_code'] = $door_code;

        return $this;
    }

    /**
     * Gets email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->container['email'];
    }

    /**
     * Sets email
     *
     * @param string $email email
     *
     * @return $this
     */
    public function setEmail($email)
    {
        $this->container['email'] = $email;

        return $this;
    }

    /**
     * Gets fax
     *
     * @return string
     */
    public function getFax()
    {
        return $this->container['fax'];
    }

    /**
     * Sets fax
     *
     * @param string $fax fax
     *
     * @return $this
     */
    public function setFax($fax)
    {
        $this->container['fax'] = $fax;

        return $this;
    }

    /**
     * Gets fields
     *
     * @return map[string,object]
     */
    public function getFields()
    {
        return $this->container['fields'];
    }

    /**
     * Sets fields
     *
     * @param map[string,object] $fields fields
     *
     * @return $this
     */
    public function setFields($fields)
    {
        $this->container['fields'] = $fields;

        return $this;
    }

    /**
     * Gets first_name
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->container['first_name'];
    }

    /**
     * Sets first_name
     *
     * @param string $first_name first_name
     *
     * @return $this
     */
    public function setFirstName($first_name)
    {
        $this->container['first_name'] = $first_name;

        return $this;
    }

    /**
     * Gets floor
     *
     * @return string
     */
    public function getFloor()
    {
        return $this->container['floor'];
    }

    /**
     * Sets floor
     *
     * @param string $floor floor
     *
     * @return $this
     */
    public function setFloor($floor)
    {
        $this->container['floor'] = $floor;

        return $this;
    }

    /**
     * Gets language
     *
     * @return string
     */
    public function getLanguage()
    {
        return $this->container['language'];
    }

    /**
     * Sets language
     *
     * @param string $language language
     *
     * @return $this
     */
    public function setLanguage($language)
    {
        $this->container['language'] = $language;

        return $this;
    }

    /**
     * Gets last_name
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->container['last_name'];
    }

    /**
     * Sets last_name
     *
     * @param string $last_name last_name
     *
     * @return $this
     */
    public function setLastName($last_name)
    {
        $this->container['last_name'] = $last_name;

        return $this;
    }

    /**
     * Gets mobile
     *
     * @return string
     */
    public function getMobile()
    {
        return $this->container['mobile'];
    }

    /**
     * Sets mobile
     *
     * @param string $mobile mobile
     *
     * @return $this
     */
    public function setMobile($mobile)
    {
        $this->container['mobile'] = $mobile;

        return $this;
    }

    /**
     * Gets not_valid
     *
     * @return bool
     */
    public function getNotValid()
    {
        return $this->container['not_valid'];
    }

    /**
     * Sets not_valid
     *
     * @param bool $not_valid not_valid
     *
     * @return $this
     */
    public function setNotValid($not_valid)
    {
        $this->container['not_valid'] = $not_valid;

        return $this;
    }

    /**
     * Gets number
     *
     * @return string
     */
    public function getNumber()
    {
        return $this->container['number'];
    }

    /**
     * Sets number
     *
     * @param string $number number
     *
     * @return $this
     */
    public function setNumber($number)
    {
        $this->container['number'] = $number;

        return $this;
    }

    /**
     * Gets phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->container['phone'];
    }

    /**
     * Sets phone
     *
     * @param string $phone phone
     *
     * @return $this
     */
    public function setPhone($phone)
    {
        $this->container['phone'] = $phone;

        return $this;
    }

    /**
     * Gets state
     *
     * @return string
     */
    public function getState()
    {
        return $this->container['state'];
    }

    /**
     * Sets state
     *
     * @param string $state state
     *
     * @return $this
     */
    public function setState($state)
    {
        $this->container['state'] = $state;

        return $this;
    }

    /**
     * Gets style
     *
     * @return \PlanningPme\Model\PlanningPMEAPIModelsStyleModel
     */
    public function getStyle()
    {
        return $this->container['style'];
    }

    /**
     * Sets style
     *
     * @param \PlanningPme\Model\PlanningPMEAPIModelsStyleModel $style style
     *
     * @return $this
     */
    public function setStyle($style)
    {
        $this->container['style'] = $style;

        return $this;
    }

    /**
     * Gets title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->container['title'];
    }

    /**
     * Sets title
     *
     * @param string $title Miss, Mr, Ms
     *
     * @return $this
     */
    public function setTitle($title)
    {
        $allowedValues = $this->getTitleAllowableValues();
        if (!is_null($title) && !in_array($title, $allowedValues, true)) {
            throw new \InvalidArgumentException(
                sprintf(
                    "Invalid value for 'title', must be one of '%s'",
                    implode("', '", $allowedValues)
                )
            );
        }
        $this->container['title'] = $title;

        return $this;
    }

    /**
     * Gets type
     *
     * @return string
     */
    public function getType()
    {
        return $this->container['type'];
    }

    /**
     * Sets type
     *
     * @param string $type Company, Individual
     *
     * @return $this
     */
    public function setType($type)
    {
        $allowedValues = $this->getTypeAllowableValues();
        if (!is_null($type) && !in_array($type, $allowedValues, true)) {
            throw new \InvalidArgumentException(
                sprintf(
                    "Invalid value for 'type', must be one of '%s'",
                    implode("', '", $allowedValues)
                )
            );
        }
        $this->container['type'] = $type;

        return $this;
    }

    /**
     * Gets web_site
     *
     * @return string
     */
    public function getWebSite()
    {
        return $this->container['web_site'];
    }

    /**
     * Sets web_site
     *
     * @param string $web_site web_site
     *
     * @return $this
     */
    public function setWebSite($web_site)
    {
        $this->container['web_site'] = $web_site;

        return $this;
    }

    /**
     * Gets zip
     *
     * @return string
     */
    public function getZip()
    {
        return $this->container['zip'];
    }

    /**
     * Sets zip
     *
     * @param string $zip zip
     *
     * @return $this
     */
    public function setZip($zip)
    {
        $this->container['zip'] = $zip;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    #[\ReturnTypeWillChange]
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    #[\ReturnTypeWillChange]
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}
