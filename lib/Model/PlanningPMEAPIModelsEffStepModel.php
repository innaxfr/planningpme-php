<?php
/**
 * PlanningPMEAPIModelsEffStepModel
 *
 * PHP version 5
 *
 * @category Class
 * @package  PlanningPme
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * PlanningPME.API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v4-7-0-27
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.64
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace PlanningPme\Model;

use \ArrayAccess;
use \PlanningPme\ObjectSerializer;

/**
 * PlanningPMEAPIModelsEffStepModel Class Doc Comment
 *
 * @category Class
 * @package  PlanningPme
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class PlanningPMEAPIModelsEffStepModel implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $swaggerModelName = 'PlanningPME.API.Models.EffStepModel';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = [
        'gap' => 'int',
        'gap_unit' => 'string',
        'id' => 'int',
        'step_type' => 'string'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerFormats = [
        'gap' => 'int32',
        'gap_unit' => null,
        'id' => 'int32',
        'step_type' => null
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'gap' => 'gap',
        'gap_unit' => 'gapUnit',
        'id' => 'id',
        'step_type' => 'stepType'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'gap' => 'setGap',
        'gap_unit' => 'setGapUnit',
        'id' => 'setId',
        'step_type' => 'setStepType'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'gap' => 'getGap',
        'gap_unit' => 'getGapUnit',
        'id' => 'getId',
        'step_type' => 'getStepType'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$swaggerModelName;
    }

    const GAP_UNIT_MINUTE = 'Minute';
    const GAP_UNIT_HOUR = 'Hour';
    const GAP_UNIT_DAY = 'Day';
    const GAP_UNIT_WEEK = 'Week';
    const GAP_UNIT_MONTH = 'Month';
    const GAP_UNIT_YEAR = 'Year';
    const STEP_TYPE_INSERT = 'Insert';
    const STEP_TYPE_DELETE = 'Delete';
    const STEP_TYPE_UPDATE = 'Update';
    const STEP_TYPE_UNPERIODIZE = 'Unperiodize';
    const STEP_TYPE_START = 'Start';
    const STEP_TYPE_END = 'End';

    /**
     * Gets allowable values of the enum
     *
     * @return string[]
     */
    public function getGapUnitAllowableValues()
    {
        return [
            self::GAP_UNIT_MINUTE,
            self::GAP_UNIT_HOUR,
            self::GAP_UNIT_DAY,
            self::GAP_UNIT_WEEK,
            self::GAP_UNIT_MONTH,
            self::GAP_UNIT_YEAR,
        ];
    }
    /**
     * Gets allowable values of the enum
     *
     * @return string[]
     */
    public function getStepTypeAllowableValues()
    {
        return [
            self::STEP_TYPE_INSERT
            self::STEP_TYPE_DELETE
            self::STEP_TYPE_UPDATE
            self::STEP_TYPE_UNPERIODIZE
            self::STEP_TYPE_START
            self::STEP_TYPE_END
        ];
    }

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['gap'] = isset($data['gap']) ? $data['gap'] : null;
        $this->container['gap_unit'] = isset($data['gap_unit']) ? $data['gap_unit'] : null;
        $this->container['id'] = isset($data['id']) ? $data['id'] : null;
        $this->container['step_type'] = isset($data['step_type']) ? $data['step_type'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        $allowedValues = $this->getGapUnitAllowableValues();
        if (!is_null($this->container['gap_unit']) && !in_array($this->container['gap_unit'], $allowedValues, true)) {
            $invalidProperties[] = sprintf(
                "invalid value for 'gap_unit', must be one of '%s'",
                implode("', '", $allowedValues)
            );
        }

        $allowedValues = $this->getStepTypeAllowableValues();
        if (!is_null($this->container['step_type']) && !in_array($this->container['step_type'], $allowedValues, true)) {
            $invalidProperties[] = sprintf(
                "invalid value for 'step_type', must be one of '%s'",
                implode("', '", $allowedValues)
            );
        }

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets gap
     *
     * @return int
     */
    public function getGap()
    {
        return $this->container['gap'];
    }

    /**
     * Sets gap
     *
     * @param int $gap gap
     *
     * @return $this
     */
    public function setGap($gap)
    {
        $this->container['gap'] = $gap;

        return $this;
    }

    /**
     * Gets gap_unit
     *
     * @return string
     */
    public function getGapUnit()
    {
        return $this->container['gap_unit'];
    }

    /**
     * Sets gap_unit
     *
     * @param string $gap_unit Minute, Hour, Day, Week, Month, Year
     *
     * @return $this
     */
    public function setGapUnit($gap_unit)
    {
        $allowedValues = $this->getGapUnitAllowableValues();
        if (!is_null($gap_unit) && !in_array($gap_unit, $allowedValues, true)) {
            throw new \InvalidArgumentException(
                sprintf(
                    "Invalid value for 'gap_unit', must be one of '%s'",
                    implode("', '", $allowedValues)
                )
            );
        }
        $this->container['gap_unit'] = $gap_unit;

        return $this;
    }

    /**
     * Gets id
     *
     * @return int
     */
    public function getId()
    {
        return $this->container['id'];
    }

    /**
     * Sets id
     *
     * @param int $id id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->container['id'] = $id;

        return $this;
    }

    /**
     * Gets step_type
     *
     * @return string
     */
    public function getStepType()
    {
        return $this->container['step_type'];
    }

    /**
     * Sets step_type
     *
     * @param string $step_type Insert, Delete, Update, Unperiodize, Start, End
     *
     * @return $this
     */
    public function setStepType($step_type)
    {
        $allowedValues = $this->getStepTypeAllowableValues();
        if (!is_null($step_type) && !in_array($step_type, $allowedValues, true)) {
            throw new \InvalidArgumentException(
                sprintf(
                    "Invalid value for 'step_type', must be one of '%s'",
                    implode("', '", $allowedValues)
                )
            );
        }
        $this->container['step_type'] = $step_type;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    #[\ReturnTypeWillChange]
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    #[\ReturnTypeWillChange]
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}
