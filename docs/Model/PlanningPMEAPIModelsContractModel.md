# PlanningPMEAPIModelsContractModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**beginning_date** | [**\DateTime**](\DateTime.md) |  | 
**end_date** | [**\DateTime**](\DateTime.md) |  | 
**key** | **int** |  | 
**label** | **string** |  | 
**resource_key** | **int** |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

