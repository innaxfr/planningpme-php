# PlanningPMEAPIModelsDoModelPlaceModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address** | **string** |  | [optional] 
**city** | **string** |  | [optional] 
**door_code** | **string** |  | [optional] 
**floor** | **string** |  | [optional] 
**zip** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

