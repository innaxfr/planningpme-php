# PlanningPMEAPIModelsEffSourceModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**source_id** | **int** |  | [optional] 
**source_type** | **string** | Customer, Resource, Project, Assignment, Task, Unavailability, Status, ResourceIn, ProjectIn, TaskIn | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

