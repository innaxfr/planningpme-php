# PlanningPMEAPIModelsPostFormatLangModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**date_time_format** | [**\PlanningPme\Model\PlanningPMECoreDataDateTimeFormat**](PlanningPMECoreDataDateTimeFormat.md) |  | 
**labels** | [**\PlanningPme\Model\PlanningPMEAPIModelsPostFormatLangModelLabels**](PlanningPMEAPIModelsPostFormatLangModelLabels.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

