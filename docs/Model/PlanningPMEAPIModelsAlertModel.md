# PlanningPMEAPIModelsAlertModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | **int** |  | 
**occurence_start** | [**\DateTime**](\DateTime.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

