# PlanningPMEAPIModelsScheduleModelDayModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**active** | **bool** |  | 
**am** | **bool** |  | [optional] 
**end_afternoon** | [**\PlanningPme\Model\PlanningPMEAPIModelsTimeOnlyModel**](PlanningPMEAPIModelsTimeOnlyModel.md) |  | [optional] 
**end_morning** | [**\PlanningPme\Model\PlanningPMEAPIModelsTimeOnlyModel**](PlanningPMEAPIModelsTimeOnlyModel.md) |  | [optional] 
**hours** | **double** |  | [optional] 
**pm** | **bool** |  | [optional] 
**start_afternoon** | [**\PlanningPme\Model\PlanningPMEAPIModelsTimeOnlyModel**](PlanningPMEAPIModelsTimeOnlyModel.md) |  | [optional] 
**start_morning** | [**\PlanningPme\Model\PlanningPMEAPIModelsTimeOnlyModel**](PlanningPMEAPIModelsTimeOnlyModel.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

