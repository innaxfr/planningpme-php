# PlanningPMEAPIModelsUserDepartmentModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access** | **string** | All, Read, Write | 
**department** | [**\PlanningPme\Model\PlanningPMEAPIModelsKeyModel**](PlanningPMEAPIModelsKeyModel.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

