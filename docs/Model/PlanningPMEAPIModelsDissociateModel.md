# PlanningPMEAPIModelsDissociateModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | **int** |  | 
**resource_key** | **int** |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

