# PlanningPMEAPIModelsGroupModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access** | [**map[string,map[string,bool]]**](map.md) |  | [optional] 
**create** | **map[string,bool]** |  | [optional] 
**delete** | **map[string,bool]** |  | [optional] 
**label** | **string** |  | [optional] 
**mandatory** | [**map[string,map[string,bool]]**](map.md) |  | [optional] 
**read** | **map[string,bool]** |  | [optional] 
**rights** | **map[string,bool]** |  | [optional] 
**tabs** | [**map[string,map[string,bool]]**](map.md) |  | [optional] 
**time_restricted_view** | [**\PlanningPme\Model\PlanningPMECoreDataTimeLapseScope**](PlanningPMECoreDataTimeLapseScope.md) |  | [optional] 
**update** | **map[string,bool]** |  | [optional] 
**visible** | [**map[string,map[string,bool]]**](map.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

