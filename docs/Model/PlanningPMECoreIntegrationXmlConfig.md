# PlanningPMECoreIntegrationXmlConfig

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**in** | [**\PlanningPme\Model\PlanningPMECoreIntegrationXmlIn**](PlanningPMECoreIntegrationXmlIn.md) |  | [optional] 
**name** | **string** |  | [optional] 
**out** | [**\PlanningPme\Model\PlanningPMECoreIntegrationXmlOut**](PlanningPMECoreIntegrationXmlOut.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

