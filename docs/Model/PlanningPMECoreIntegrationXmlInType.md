# PlanningPMECoreIntegrationXmlInType

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**defaults** | [**\PlanningPme\Model\PlanningPMECoreIntegrationXmlInDefault[]**](PlanningPMECoreIntegrationXmlInDefault.md) |  | [optional] 
**key_property** | **string** |  | [optional] 
**name** | **string** |  | [optional] 
**should_insert** | **bool** |  | [optional] 
**should_update** | **bool** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

