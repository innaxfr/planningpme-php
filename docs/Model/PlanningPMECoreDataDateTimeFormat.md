# PlanningPMECoreDataDateTimeFormat

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**long_date** | **string** |  | 
**minute_step** | **int** |  | 
**short_date** | **string** |  | 
**time** | **string** |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

