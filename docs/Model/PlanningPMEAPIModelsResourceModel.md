# PlanningPMEAPIModelsResourceModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address** | **string** |  | [optional] 
**city** | **string** |  | [optional] 
**contracts** | [**\PlanningPme\Model\PlanningPMEAPIModelsContractModel[]**](PlanningPMEAPIModelsContractModel.md) |  | [optional] 
**cost** | **double** |  | [optional] 
**country** | **string** |  | [optional] 
**departments** | [**\PlanningPme\Model\PlanningPMEAPIModelsKeyModel[]**](PlanningPMEAPIModelsKeyModel.md) |  | [optional] 
**email** | **string** |  | [optional] 
**fields** | **map[string,object]** |  | [optional] 
**first_name** | **string** |  | [optional] 
**fix** | **bool** |  | [optional] 
**google_calendar_sync** | [**\PlanningPme\Model\PlanningPMEAPIModelsGoogleCalendarSyncModel**](PlanningPMEAPIModelsGoogleCalendarSyncModel.md) |  | [optional] 
**label** | **string** |  | [optional] 
**language** | **string** |  | [optional] 
**last_name** | **string** |  | [optional] 
**manager** | [**\PlanningPme\Model\PlanningPMEAPIModelsKeyModel**](PlanningPMEAPIModelsKeyModel.md) |  | [optional] 
**mobile** | **string** |  | [optional] 
**not_valid** | **bool** |  | [optional] 
**phone** | **string** |  | [optional] 
**registration_number** | **string** |  | [optional] 
**schedule** | [**\PlanningPme\Model\PlanningPMEAPIModelsKeyModel**](PlanningPMEAPIModelsKeyModel.md) |  | [optional] 
**skills** | [**\PlanningPme\Model\PlanningPMEAPIModelsKeyModel[]**](PlanningPMEAPIModelsKeyModel.md) |  | [optional] 
**state** | **string** |  | [optional] 
**style** | [**\PlanningPme\Model\PlanningPMEAPIModelsStyleModel**](PlanningPMEAPIModelsStyleModel.md) |  | [optional] 
**timezone** | **string** |  | [optional] 
**type** | **string** | Human, Material, ToPlan, Extern | [optional] 
**zip** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

