# PlanningPMEAPIModelsEffStepModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**gap** | **int** |  | [optional] 
**gap_unit** | **string** | Minute, Hour, Day, Week, Month, Year | [optional] 
**id** | **int** |  | [optional] 
**step_type** | **string** | Insert, Delete, Update, Unperiodize, Start, End | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

