# PlanningPMEAPIModelsProjectArrangeModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ignore_secondary_validation** | **bool** |  | [optional] 
**resources** | [**\PlanningPme\Model\PlanningPMEAPIModelsKeyModel[]**](PlanningPMEAPIModelsKeyModel.md) |  | 
**start_date** | [**\DateTime**](\DateTime.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

