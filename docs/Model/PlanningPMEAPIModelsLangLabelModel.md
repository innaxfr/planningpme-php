# PlanningPMEAPIModelsLangLabelModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**label** | **string** |  | 
**type** | **string** | Customer, Department, Equipment, Project, Resource, Skill, Slot1, Slot2, Slot3, Slot4, Category, SubProject, Task, TaskStatus, Unavailability, UnavailabilityStatus, TabAddress, TabCustomer1, TabCustomer2, TabCustomer3, TabEquipment0, TabHumanResource1, TabHumanResource2, TabHumanResource3, TabMaterialResource1, TabMaterialResource2, TabMaterialResource3, TabProject1, TabProject2, TabProject3, TabTask1, TabTask2, TabTask3, TabTask4, TabTask5, ShortDate, LongDate, Time, NewTask, NewUnavailability, Assignment, NewAssignment, NewProject | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

