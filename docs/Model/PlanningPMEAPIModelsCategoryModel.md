# PlanningPMEAPIModelsCategoryModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**color_hatch** | **string** |  | [optional] 
**label** | **string** |  | [optional] 
**style** | [**\PlanningPme\Model\PlanningPMEAPIModelsStyleModel**](PlanningPMEAPIModelsStyleModel.md) |  | [optional] 
**type_hatch** | **string** | BDIAGONAL, CROSS, DIAGCROSS, FDIAGONAL, HORIZONTAL, VERTICAL | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

