# PlanningPMEAPIModelsEffConditionModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**order** | **int** |  | [optional] 
**sources** | [**\PlanningPme\Model\PlanningPMEAPIModelsEffSourceModel[]**](PlanningPMEAPIModelsEffSourceModel.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

