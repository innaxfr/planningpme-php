# PlanningPMEAPIModelsDepartmentModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**label** | **string** |  | [optional] 
**parent** | [**\PlanningPme\Model\PlanningPMEAPIModelsKeyModel**](PlanningPMEAPIModelsKeyModel.md) |  | [optional] 
**resources** | [**\PlanningPme\Model\PlanningPMEAPIModelsKeyModel[]**](PlanningPMEAPIModelsKeyModel.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

