# PlanningPMEAPIModelsPublicHolidayModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**day** | [**\DateTime**](\DateTime.md) |  | 
**each_year** | **bool** |  | 
**label** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

