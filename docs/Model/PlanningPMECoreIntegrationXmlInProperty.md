# PlanningPMECoreIntegrationXmlInProperty

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**field** | **string** |  | [optional] 
**format** | **string** |  | [optional] 
**name** | **string** |  | [optional] 
**ref_field** | **string** |  | [optional] 
**switch** | [**\PlanningPme\Model\PlanningPMECoreIntegrationXmlInSwitch**](PlanningPMECoreIntegrationXmlInSwitch.md) |  | [optional] 
**transform** | **string** |  | [optional] 
**type** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

