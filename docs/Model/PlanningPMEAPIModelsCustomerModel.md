# PlanningPMEAPIModelsCustomerModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address** | **string** |  | [optional] 
**building** | **string** |  | [optional] 
**city** | **string** |  | [optional] 
**company** | **string** |  | [optional] 
**country** | **string** |  | [optional] 
**door_code** | **string** |  | [optional] 
**email** | **string** |  | [optional] 
**fax** | **string** |  | [optional] 
**fields** | **map[string,object]** |  | [optional] 
**first_name** | **string** |  | [optional] 
**floor** | **string** |  | [optional] 
**language** | **string** |  | [optional] 
**last_name** | **string** |  | [optional] 
**mobile** | **string** |  | [optional] 
**not_valid** | **bool** |  | [optional] 
**number** | **string** |  | [optional] 
**phone** | **string** |  | [optional] 
**state** | **string** |  | [optional] 
**style** | [**\PlanningPme\Model\PlanningPMEAPIModelsStyleModel**](PlanningPMEAPIModelsStyleModel.md) |  | [optional] 
**title** | **string** | Miss, Mr, Ms | [optional] 
**type** | **string** | Company, Individual | [optional] 
**web_site** | **string** |  | [optional] 
**zip** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

