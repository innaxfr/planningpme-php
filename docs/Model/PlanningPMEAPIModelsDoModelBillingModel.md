# PlanningPMEAPIModelsDoModelBillingModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**amount** | **string** |  | [optional] 
**bill** | **string** |  | [optional] 
**bill_comment** | **string** |  | [optional] 
**billing_type** | **string** | Package, Unit | [optional] 
**estimate** | **string** |  | [optional] 
**order** | **string** |  | [optional] 
**paid** | **bool** |  | [optional] 
**unit_price** | **double** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

