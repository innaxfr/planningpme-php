# PlanningPMECoreIntegrationXmlInCase

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**thens** | [**\PlanningPme\Model\PlanningPMECoreIntegrationXmlInThen[]**](PlanningPMECoreIntegrationXmlInThen.md) |  | [optional] 
**value** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

