# PlanningPMEAPIModelsSubProjectModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fields** | **map[string,object]** |  | [optional] 
**label** | **string** |  | [optional] 
**project** | [**\PlanningPme\Model\PlanningPMEAPIModelsKeyModel**](PlanningPMEAPIModelsKeyModel.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

