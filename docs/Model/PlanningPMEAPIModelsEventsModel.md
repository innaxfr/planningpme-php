# PlanningPMEAPIModelsEventsModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customer_key** | **int** |  | [optional] 
**load_occurrences** | **bool** |  | [optional] 
**name** | **string** |  | [optional] 
**page_index** | **int** |  | [optional] 
**page_size** | **int** |  | [optional] 
**project_key** | **int** |  | [optional] 
**resource_key** | **int** |  | [optional] 
**sub_project_key** | **int** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

