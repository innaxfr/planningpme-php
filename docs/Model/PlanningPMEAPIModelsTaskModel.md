# PlanningPMEAPIModelsTaskModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**all_departments** | **bool** |  | [optional] 
**departments** | [**\PlanningPme\Model\PlanningPMEAPIModelsKeyModel[]**](PlanningPMEAPIModelsKeyModel.md) |  | [optional] 
**duration** | **double** |  | [optional] 
**label** | **string** |  | [optional] 
**skills** | [**\PlanningPme\Model\PlanningPMEAPIModelsKeyModel[][]**](array.md) |  | [optional] 
**style** | [**\PlanningPme\Model\PlanningPMEAPIModelsStyleModel**](PlanningPMEAPIModelsStyleModel.md) |  | [optional] 
**time_from** | [**\PlanningPme\Model\PlanningPMEAPIModelsTimeOnlyModel**](PlanningPMEAPIModelsTimeOnlyModel.md) |  | [optional] 
**time_to** | [**\PlanningPme\Model\PlanningPMEAPIModelsTimeOnlyModel**](PlanningPMEAPIModelsTimeOnlyModel.md) |  | [optional] 
**type** | **string** | Default, Duration, Time | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

