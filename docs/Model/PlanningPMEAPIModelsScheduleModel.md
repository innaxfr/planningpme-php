# PlanningPMEAPIModelsScheduleModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count_every_hour** | **bool** |  | [optional] 
**days** | [**\PlanningPme\Model\PlanningPMEAPIModelsScheduleModelDayModel[]**](PlanningPMEAPIModelsScheduleModelDayModel.md) |  | 
**is_default** | **bool** |  | 
**label** | **string** |  | [optional] 
**work_capacity** | **string** | Hours, Slots | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

