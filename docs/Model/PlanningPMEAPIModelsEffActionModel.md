# PlanningPMEAPIModelsEffActionModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**action_data** | **string** |  | [optional] 
**action_type** | **string** | Email, Sms, MicrosoftOutlook, GoogleCalendar | [optional] 
**id** | **int** |  | [optional] 
**if_op** | **string** | IsIn, BecomesIn | [optional] 
**if_type** | **string** | Category, Status | [optional] 
**if_values** | **int[]** |  | [optional] 
**order** | **int** |  | [optional] 
**steps** | [**\PlanningPme\Model\PlanningPMEAPIModelsEffStepModel[]**](PlanningPMEAPIModelsEffStepModel.md) |  | 
**target_ids** | **int[]** |  | [optional] 
**target_type** | **string** | Customer, Resource, User, Account | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

