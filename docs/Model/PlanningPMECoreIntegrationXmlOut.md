# PlanningPMECoreIntegrationXmlOut

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**properties** | [**\PlanningPme\Model\PlanningPMECoreIntegrationXmlOutProperty[]**](PlanningPMECoreIntegrationXmlOutProperty.md) |  | [optional] 
**root_type** | **string** | Customer, Project, Resource | [optional] 
**types** | [**\PlanningPme\Model\PlanningPMECoreIntegrationXmlOutType[]**](PlanningPMECoreIntegrationXmlOutType.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

