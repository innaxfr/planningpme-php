# PlanningPMEAPIModelsEffDefinitionModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**actions** | [**\PlanningPme\Model\PlanningPMEAPIModelsEffActionModel[]**](PlanningPMEAPIModelsEffActionModel.md) |  | [optional] 
**conditions** | [**\PlanningPme\Model\PlanningPMEAPIModelsEffConditionModel[]**](PlanningPMEAPIModelsEffConditionModel.md) |  | [optional] 
**enabled** | **bool** |  | [optional] 
**name** | **string** |  | 
**subject_id** | **int** |  | [optional] 
**subject_type** | **string** | Assignment, Task, Unavailability | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

