# PlanningPMEAPIModelsExportModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**assignments** | **int[]** |  | [optional] 
**by_day** | **bool** |  | 
**by_department** | **bool** |  | 
**categories** | **int[]** |  | [optional] 
**customers** | **int[]** |  | [optional] 
**date_from** | [**\DateTime**](\DateTime.md) |  | 
**date_to** | [**\DateTime**](\DateTime.md) |  | 
**departments** | **int[]** |  | [optional] 
**filters** | [**map[string,string[]]**](array.md) |  | [optional] 
**projects** | **int[]** |  | [optional] 
**resources** | **int[]** |  | [optional] 
**skills** | **int[]** |  | [optional] 
**sub_projects** | **int[]** |  | [optional] 
**tasks** | **int[]** |  | [optional] 
**task_statutes** | **int[]** |  | [optional] 
**unavailabilities** | **int[]** |  | [optional] 
**unavailability_statutes** | **int[]** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

