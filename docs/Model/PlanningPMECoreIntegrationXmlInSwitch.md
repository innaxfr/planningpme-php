# PlanningPMECoreIntegrationXmlInSwitch

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cases** | [**\PlanningPme\Model\PlanningPMECoreIntegrationXmlInCase[]**](PlanningPMECoreIntegrationXmlInCase.md) |  | [optional] 
**default** | [**\PlanningPme\Model\PlanningPMECoreIntegrationXmlInCase**](PlanningPMECoreIntegrationXmlInCase.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

