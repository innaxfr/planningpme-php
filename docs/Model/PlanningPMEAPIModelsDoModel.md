# PlanningPMEAPIModelsDoModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**all_day** | **bool** |  | [optional] 
**allocation** | **int** |  | [optional] 
**billing** | [**\PlanningPme\Model\PlanningPMEAPIModelsDoModelBillingModel**](PlanningPMEAPIModelsDoModelBillingModel.md) |  | [optional] 
**break** | [**\PlanningPme\Model\PlanningPMEAPIModelsTimeOnlyModel**](PlanningPMEAPIModelsTimeOnlyModel.md) |  | [optional] 
**category** | [**\PlanningPme\Model\PlanningPMEAPIModelsKeyModel**](PlanningPMEAPIModelsKeyModel.md) |  | [optional] 
**class** | **string** | Task, Unavailability, Assignment | [optional] 
**customers** | [**\PlanningPme\Model\PlanningPMEAPIModelsKeyModel[]**](PlanningPMEAPIModelsKeyModel.md) |  | [optional] 
**duration** | [**\PlanningPme\Model\PlanningPMECoreDataDayTime**](PlanningPMECoreDataDayTime.md) |  | [optional] 
**end** | [**\DateTime**](\DateTime.md) |  | [optional] 
**equipment** | [**\PlanningPme\Model\PlanningPMEAPIModelsKeyModel**](PlanningPMEAPIModelsKeyModel.md) |  | [optional] 
**fields** | **map[string,object]** |  | [optional] 
**has_recurrence** | **bool** |  | [optional] 
**ignore_secondary_validation** | **bool** |  | [optional] 
**is_assignment** | **bool** |  | [optional] 
**is_unavailability** | **bool** |  | [optional] 
**label** | **string** |  | [optional] 
**mode** | **string** | None, End, Duration | [optional] 
**old_recurrence** | [**\PlanningPme\Model\PlanningPMEAPIModelsDoModelOldRecurrenceModel**](PlanningPMEAPIModelsDoModelOldRecurrenceModel.md) |  | [optional] 
**place** | [**\PlanningPme\Model\PlanningPMEAPIModelsDoModelPlaceModel**](PlanningPMEAPIModelsDoModelPlaceModel.md) |  | [optional] 
**project** | [**\PlanningPme\Model\PlanningPMEAPIModelsKeyModel**](PlanningPMEAPIModelsKeyModel.md) |  | [optional] 
**recurrence** | [**\PlanningPme\Model\PlanningPMEAPIModelsDoModelRecurrenceModel**](PlanningPMEAPIModelsDoModelRecurrenceModel.md) |  | [optional] 
**remark** | **string** |  | [optional] 
**resources** | [**\PlanningPme\Model\PlanningPMEAPIModelsKeyModel[]**](PlanningPMEAPIModelsKeyModel.md) |  | [optional] 
**start** | [**\DateTime**](\DateTime.md) |  | [optional] 
**status** | [**\PlanningPme\Model\PlanningPMEAPIModelsKeyModel**](PlanningPMEAPIModelsKeyModel.md) |  | [optional] 
**sub_project** | [**\PlanningPme\Model\PlanningPMEAPIModelsKeyModel**](PlanningPMEAPIModelsKeyModel.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

