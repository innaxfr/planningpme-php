# PlanningPMEAPIModelsUserModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customers** | [**\PlanningPme\Model\PlanningPMEAPIModelsKeyModel[]**](PlanningPMEAPIModelsKeyModel.md) |  | [optional] 
**departments** | [**\PlanningPme\Model\PlanningPMEAPIModelsUserDepartmentModel[]**](PlanningPMEAPIModelsUserDepartmentModel.md) |  | [optional] 
**email** | **string** |  | [optional] 
**group** | [**\PlanningPme\Model\PlanningPMEAPIModelsKeyModel**](PlanningPMEAPIModelsKeyModel.md) |  | 
**has_integration_api_access** | **bool** |  | [optional] 
**has_mobile_access** | **bool** |  | [optional] 
**language** | **string** |  | [optional] 
**login** | **string** |  | 
**new_password** | **string** |  | [optional] 
**resource** | [**\PlanningPme\Model\PlanningPMEAPIModelsKeyModel**](PlanningPMEAPIModelsKeyModel.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

