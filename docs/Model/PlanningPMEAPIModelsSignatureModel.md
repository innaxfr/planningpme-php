# PlanningPMEAPIModelsSignatureModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**content_type** | **string** |  | 
**data** | **string** |  | 
**field** | **string** |  | 
**owner_key** | **int** |  | 
**owner_type** | **string** | Task, Customer, Equipment, Resource, Project, SubProject | 
**person** | **string** |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

