# PlanningPMECoreDataDayTime

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**day** | **int** |  | 
**time** | [**\PlanningPme\Model\PlanningPMECoreDataTime**](PlanningPMECoreDataTime.md) |  | 
**unit** | **string** | Day, Time | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

