# PlanningPMEAPIModelsVacationModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count_holidays** | **string** | Five, Six | [optional] 
**date_holidays** | [**\DateTime**](\DateTime.md) |  | [optional] 
**full_day_holidays** | **bool** |  | [optional] 
**has_vacations** | **bool** |  | [optional] 
**label_holidays** | **string** |  | [optional] 
**label_vacations** | **string** |  | [optional] 
**number_days_holidays** | **int** |  | [optional] 
**number_days_vacations** | **int** |  | [optional] 
**start_month_vacations** | **int** |  | [optional] 
**unavailability_counter_decimal** | **bool** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

