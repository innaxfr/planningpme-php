# PlanningPMECoreIntegrationXmlIn

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**properties** | [**\PlanningPme\Model\PlanningPMECoreIntegrationXmlInProperty[]**](PlanningPMECoreIntegrationXmlInProperty.md) |  | [optional] 
**root_type** | **string** | Customer, Project, Resource | [optional] 
**types** | [**\PlanningPme\Model\PlanningPMECoreIntegrationXmlInType[]**](PlanningPMECoreIntegrationXmlInType.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

