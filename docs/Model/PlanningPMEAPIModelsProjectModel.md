# PlanningPMEAPIModelsProjectModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**all_departments** | **bool** |  | [optional] 
**customer** | [**\PlanningPme\Model\PlanningPMEAPIModelsKeyModel**](PlanningPMEAPIModelsKeyModel.md) |  | [optional] 
**departments** | [**\PlanningPme\Model\PlanningPMEAPIModelsKeyModel[]**](PlanningPMEAPIModelsKeyModel.md) |  | [optional] 
**fields** | **map[string,object]** |  | [optional] 
**label** | **string** |  | [optional] 
**not_valid** | **bool** |  | [optional] 
**style** | [**\PlanningPme\Model\PlanningPMEAPIModelsStyleModel**](PlanningPMEAPIModelsStyleModel.md) |  | [optional] 
**sub_projects** | [**\PlanningPme\Model\PlanningPMEAPIModelsKeyLabelModel[]**](PlanningPMEAPIModelsKeyLabelModel.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

