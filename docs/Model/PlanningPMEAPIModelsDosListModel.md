# PlanningPMEAPIModelsDosListModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**assignments** | **int[]** |  | [optional] 
**categories** | **int[]** |  | [optional] 
**customers** | **int[]** |  | [optional] 
**date_from** | [**\DateTime**](\DateTime.md) |  | [optional] 
**date_to** | [**\DateTime**](\DateTime.md) |  | [optional] 
**departments** | **int[]** |  | [optional] 
**filters** | [**map[string,string[]]**](array.md) |  | [optional] 
**page_index** | **int** |  | [optional] 
**page_size** | **int** |  | [optional] 
**projects** | **int[]** |  | [optional] 
**resources** | **int[]** |  | [optional] 
**results** | **string** | Task, Unavailability, Assignment | [optional] 
**skills** | **int[]** |  | [optional] 
**sub_projects** | **int[]** |  | [optional] 
**tasks** | **int[]** |  | [optional] 
**task_statutes** | **int[]** |  | [optional] 
**unavailabilities** | **int[]** |  | [optional] 
**unavailability_statutes** | **int[]** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

