# PlanningPMEAPIModelsSearchModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**end** | [**\DateTime**](\DateTime.md) |  | [optional] 
**lookup** | **string[]** |  | [optional] 
**page_index** | **int** |  | 
**page_size** | **int** |  | 
**search** | **string** |  | 
**sort_info** | **string** |  | [optional] 
**start** | [**\DateTime**](\DateTime.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

