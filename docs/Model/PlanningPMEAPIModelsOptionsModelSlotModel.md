# PlanningPMEAPIModelsOptionsModelSlotModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**color** | **string** |  | 
**end** | [**\PlanningPme\Model\PlanningPMEAPIModelsTimeOnlyModel**](PlanningPMEAPIModelsTimeOnlyModel.md) |  | 
**label** | **string** |  | 
**start** | [**\PlanningPme\Model\PlanningPMEAPIModelsTimeOnlyModel**](PlanningPMEAPIModelsTimeOnlyModel.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

