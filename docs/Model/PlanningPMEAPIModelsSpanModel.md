# PlanningPMEAPIModelsSpanModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**all_day** | **bool** |  | [optional] 
**break** | [**\PlanningPme\Model\PlanningPMECoreDataTime**](PlanningPMECoreDataTime.md) |  | [optional] 
**duration** | [**\PlanningPme\Model\PlanningPMECoreDataDayTime**](PlanningPMECoreDataDayTime.md) |  | [optional] 
**end** | [**\DateTime**](\DateTime.md) |  | [optional] 
**schedule_key** | **int** |  | 
**start** | [**\DateTime**](\DateTime.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

