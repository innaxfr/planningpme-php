# PlanningPMEAPIModelsSkillModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**domain** | [**\PlanningPme\Model\PlanningPMEAPIModelsKeyModel**](PlanningPMEAPIModelsKeyModel.md) |  | 
**name** | **string** |  | 
**parent** | [**\PlanningPme\Model\PlanningPMEAPIModelsKeyModel**](PlanningPMEAPIModelsKeyModel.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

