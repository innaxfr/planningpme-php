# PlanningPMEAPIModelsDoModelRecurrenceModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**all_day** | **bool** |  | 
**beginning_range** | [**\DateTime**](\DateTime.md) |  | 
**beginning_time** | [**\PlanningPme\Model\PlanningPMEAPIModelsTimeOnlyModel**](PlanningPMEAPIModelsTimeOnlyModel.md) |  | 
**daily_days** | **int** |  | [optional] 
**daily_type** | **string** | AllThe, AllWorkingDays | [optional] 
**day_length** | **int** |  | 
**end_range** | [**\DateTime**](\DateTime.md) |  | [optional] 
**end_time** | [**\PlanningPme\Model\PlanningPMEAPIModelsTimeOnlyModel**](PlanningPMEAPIModelsTimeOnlyModel.md) |  | 
**friday** | **bool** |  | [optional] 
**key** | **int** |  | 
**monday** | **bool** |  | [optional] 
**monthly_date** | **int** |  | [optional] 
**monthly_date_every** | **int** |  | [optional] 
**monthly_day** | **string** | Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday | [optional] 
**monthly_day_every** | **int** |  | [optional] 
**monthly_day_which** | **string** | First, Second, Third, Fourth, Last | [optional] 
**monthly_type** | **string** | Date, Day | [optional] 
**range** | **string** | NoEndDate, EndThe | 
**saturday** | **bool** |  | [optional] 
**sunday** | **bool** |  | [optional] 
**thursday** | **bool** |  | [optional] 
**tuesday** | **bool** |  | [optional] 
**type** | **string** | Daily, Weekly, Monthly, Yearly | 
**wednesday** | **bool** |  | [optional] 
**weekly_on** | **int** |  | [optional] 
**yearly_month** | **int** |  | [optional] 
**yearly_number** | **int** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

