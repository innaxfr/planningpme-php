# PlanningPMEAPIModelsFieldModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | **string** |  | [optional] 
**data_sort** | **bool** |  | [optional] 
**data_sort_desc** | **bool** |  | [optional] 
**default** | **string** |  | [optional] 
**destination** | **string** | Task0, Task1, Task2, Task3, Task4, Task5, Customer0, Customer1, Customer2, Customer3, Equipment0, HumanResource0, HumanResource1, HumanResource2, HumanResource3, MaterialResource0, MaterialResource1, MaterialResource2, MaterialResource3, Project0, Project1, Project2, Project3, SubProject0 | 
**field** | **string** |  | 
**filter** | **bool** |  | [optional] 
**height** | **int** |  | [optional] 
**label** | **string** |  | 
**length** | **int** |  | [optional] 
**mandatory** | **bool** |  | [optional] 
**mobile** | **bool** |  | [optional] 
**order** | **int** |  | [optional] 
**quick_choice** | **bool** |  | [optional] 
**readonly** | **bool** |  | [optional] 
**type** | **string** | Date, Time, Text, Numeric, Double, Combo, Link, Check, Memo, Separator, File, Position, SignatureMobile, Hyperlink, ProjectStartDate, ProjectEndDate, ProjectDuration, ProjectDeadline, ProjectDeadlineOver, Signature, ProjectEstimateDuration, SubProjectEstimateDuration, Multi | 
**width** | **int** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

