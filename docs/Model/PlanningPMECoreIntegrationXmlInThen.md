# PlanningPMECoreIntegrationXmlInThen

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**field** | **string** |  | [optional] 
**type** | **string** |  | [optional] 
**value** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

