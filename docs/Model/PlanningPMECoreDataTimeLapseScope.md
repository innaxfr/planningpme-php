# PlanningPMECoreDataTimeLapseScope

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**end** | [**\PlanningPme\Model\PlanningPMECoreDataTimeLapse**](PlanningPMECoreDataTimeLapse.md) |  | [optional] 
**start** | [**\PlanningPme\Model\PlanningPMECoreDataTimeLapse**](PlanningPMECoreDataTimeLapse.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

