# PlanningPme\ExportTemplateApi

All URIs are relative to *https://api.planningpme.com/innax*

Method | HTTP request | Description
------------- | ------------- | -------------
[**exportTemplateDelete**](ExportTemplateApi.md#exporttemplatedelete) | **DELETE** /api/exporttemplate/{id} | 
[**exportTemplateGet**](ExportTemplateApi.md#exporttemplateget) | **GET** /api/exporttemplate | 
[**exportTemplateGetAuth**](ExportTemplateApi.md#exporttemplategetauth) | **GET** /api/exporttemplate/{file}/auth | 
[**exportTemplateGetStream**](ExportTemplateApi.md#exporttemplategetstream) | **GET** /api/exporttemplate/{authId}/stream | 
[**exportTemplatePost**](ExportTemplateApi.md#exporttemplatepost) | **POST** /api/exporttemplate | 
[**exportTemplatePut**](ExportTemplateApi.md#exporttemplateput) | **PUT** /api/exporttemplate/{file}/put | 

# **exportTemplateDelete**
> object exportTemplateDelete($id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new PlanningPme\Api\ExportTemplateApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | 

try {
    $result = $apiInstance->exportTemplateDelete($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ExportTemplateApi->exportTemplateDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**|  |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **exportTemplateGet**
> object exportTemplateGet()



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new PlanningPme\Api\ExportTemplateApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->exportTemplateGet();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ExportTemplateApi->exportTemplateGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **exportTemplateGetAuth**
> object exportTemplateGetAuth($file)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new PlanningPme\Api\ExportTemplateApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$file = "file_example"; // string | 

try {
    $result = $apiInstance->exportTemplateGetAuth($file);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ExportTemplateApi->exportTemplateGetAuth: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **file** | **string**|  |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **exportTemplateGetStream**
> object exportTemplateGetStream($auth_id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new PlanningPme\Api\ExportTemplateApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$auth_id = "auth_id_example"; // string | 

try {
    $result = $apiInstance->exportTemplateGetStream($auth_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ExportTemplateApi->exportTemplateGetStream: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **auth_id** | **string**|  |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **exportTemplatePost**
> object exportTemplatePost()



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new PlanningPme\Api\ExportTemplateApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->exportTemplatePost();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ExportTemplateApi->exportTemplatePost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **exportTemplatePut**
> object exportTemplatePut($body, $file)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new PlanningPme\Api\ExportTemplateApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \PlanningPme\Model\PlanningPMEAPIModelsExportTemplateModel(); // \PlanningPme\Model\PlanningPMEAPIModelsExportTemplateModel | 
$file = "file_example"; // string | 

try {
    $result = $apiInstance->exportTemplatePut($body, $file);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ExportTemplateApi->exportTemplatePut: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\PlanningPme\Model\PlanningPMEAPIModelsExportTemplateModel**](../Model/PlanningPMEAPIModelsExportTemplateModel.md)|  |
 **file** | **string**|  |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

