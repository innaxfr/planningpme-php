# PlanningPme\FieldApi

All URIs are relative to *https://api.planningpme.com/innax*

Method | HTTP request | Description
------------- | ------------- | -------------
[**fieldDelete**](FieldApi.md#fielddelete) | **DELETE** /api/field | 
[**fieldGet**](FieldApi.md#fieldget) | **GET** /api/field | 
[**fieldGet_0**](FieldApi.md#fieldget_0) | **GET** /api/field/{id} | 
[**fieldPost**](FieldApi.md#fieldpost) | **POST** /api/field | 
[**fieldPut**](FieldApi.md#fieldput) | **PUT** /api/field/{id} | 

# **fieldDelete**
> object fieldDelete($body)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new PlanningPme\Api\FieldApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = array(56); // int[] | 

try {
    $result = $apiInstance->fieldDelete($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling FieldApi->fieldDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**int[]**](../Model/int.md)|  |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **fieldGet**
> object fieldGet($destination, $hidden, $page_index, $page_size)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new PlanningPme\Api\FieldApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$destination = "destination_example"; // string | Task0, Task1, Task2, Task3, Task4, Task5, Customer0, Customer1, Customer2, Customer3, Equipment0, HumanResource0, HumanResource1, HumanResource2, HumanResource3, MaterialResource0, MaterialResource1, MaterialResource2, MaterialResource3, Project0, Project1, Project2, Project3, SubProject0
$hidden = true; // bool | 
$page_index = 56; // int | 
$page_size = 56; // int | 

try {
    $result = $apiInstance->fieldGet($destination, $hidden, $page_index, $page_size);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling FieldApi->fieldGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **destination** | **string**| Task0, Task1, Task2, Task3, Task4, Task5, Customer0, Customer1, Customer2, Customer3, Equipment0, HumanResource0, HumanResource1, HumanResource2, HumanResource3, MaterialResource0, MaterialResource1, MaterialResource2, MaterialResource3, Project0, Project1, Project2, Project3, SubProject0 | [optional]
 **hidden** | **bool**|  | [optional]
 **page_index** | **int**|  | [optional]
 **page_size** | **int**|  | [optional]

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **fieldGet_0**
> object fieldGet_0($id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new PlanningPme\Api\FieldApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | 

try {
    $result = $apiInstance->fieldGet_0($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling FieldApi->fieldGet_0: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **fieldPost**
> object fieldPost($body)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new PlanningPme\Api\FieldApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \PlanningPme\Model\PlanningPMEAPIModelsFieldModel(); // \PlanningPme\Model\PlanningPMEAPIModelsFieldModel | 

try {
    $result = $apiInstance->fieldPost($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling FieldApi->fieldPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\PlanningPme\Model\PlanningPMEAPIModelsFieldModel**](../Model/PlanningPMEAPIModelsFieldModel.md)|  |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **fieldPut**
> object fieldPut($body, $id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new PlanningPme\Api\FieldApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \PlanningPme\Model\PlanningPMEAPIModelsFieldModel(); // \PlanningPme\Model\PlanningPMEAPIModelsFieldModel | 
$id = 56; // int | 

try {
    $result = $apiInstance->fieldPut($body, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling FieldApi->fieldPut: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\PlanningPme\Model\PlanningPMEAPIModelsFieldModel**](../Model/PlanningPMEAPIModelsFieldModel.md)|  |
 **id** | **int**|  |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

