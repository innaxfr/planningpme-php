# PlanningPme\CustomerApi

All URIs are relative to *https://api.planningpme.com/innax*

Method | HTTP request | Description
------------- | ------------- | -------------
[**customerDelete**](CustomerApi.md#customerdelete) | **DELETE** /api/customer | 
[**customerGet**](CustomerApi.md#customerget) | **GET** /api/customer | 
[**customerGet_0**](CustomerApi.md#customerget_0) | **GET** /api/customer/{id} | 
[**customerGet_1**](CustomerApi.md#customerget_1) | **GET** /api/customer/advanced | 
[**customerPost**](CustomerApi.md#customerpost) | **POST** /api/customer | 
[**customerPut**](CustomerApi.md#customerput) | **PUT** /api/customer/{id} | 

# **customerDelete**
> object customerDelete($body)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new PlanningPme\Api\CustomerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = array(56); // int[] | 

try {
    $result = $apiInstance->customerDelete($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerApi->customerDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**int[]**](../Model/int.md)|  |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customerGet**
> object customerGet($name, $projects, $advanced, $number, $label, $type, $zip, $city, $state, $country, $phone, $not_valid, $page_index, $page_size, $sort_info)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new PlanningPme\Api\CustomerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$name = "name_example"; // string | 
$projects = "projects_example"; // string | 
$advanced = true; // bool | 
$number = "number_example"; // string | 
$label = "label_example"; // string | 
$type = "type_example"; // string | Company, Individual
$zip = "zip_example"; // string | 
$city = "city_example"; // string | 
$state = "state_example"; // string | 
$country = "country_example"; // string | 
$phone = "phone_example"; // string | 
$not_valid = true; // bool | 
$page_index = 56; // int | 
$page_size = 56; // int | 
$sort_info = "sort_info_example"; // string | 

try {
    $result = $apiInstance->customerGet($name, $projects, $advanced, $number, $label, $type, $zip, $city, $state, $country, $phone, $not_valid, $page_index, $page_size, $sort_info);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerApi->customerGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **string**|  | [optional]
 **projects** | **string**|  | [optional]
 **advanced** | **bool**|  | [optional]
 **number** | **string**|  | [optional]
 **label** | **string**|  | [optional]
 **type** | **string**| Company, Individual | [optional]
 **zip** | **string**|  | [optional]
 **city** | **string**|  | [optional]
 **state** | **string**|  | [optional]
 **country** | **string**|  | [optional]
 **phone** | **string**|  | [optional]
 **not_valid** | **bool**|  | [optional]
 **page_index** | **int**|  | [optional]
 **page_size** | **int**|  | [optional]
 **sort_info** | **string**|  | [optional]

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customerGet_0**
> object customerGet_0($id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new PlanningPme\Api\CustomerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | 

try {
    $result = $apiInstance->customerGet_0($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerApi->customerGet_0: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customerGet_1**
> object customerGet_1($number, $label, $type, $zip, $city, $state, $country, $phone, $not_valid, $page_index, $page_size, $sort_info)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new PlanningPme\Api\CustomerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$number = "number_example"; // string | 
$label = "label_example"; // string | 
$type = "type_example"; // string | Company, Individual
$zip = "zip_example"; // string | 
$city = "city_example"; // string | 
$state = "state_example"; // string | 
$country = "country_example"; // string | 
$phone = "phone_example"; // string | 
$not_valid = true; // bool | 
$page_index = 56; // int | 
$page_size = 56; // int | 
$sort_info = "sort_info_example"; // string | 

try {
    $result = $apiInstance->customerGet_1($number, $label, $type, $zip, $city, $state, $country, $phone, $not_valid, $page_index, $page_size, $sort_info);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerApi->customerGet_1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **string**|  | [optional]
 **label** | **string**|  | [optional]
 **type** | **string**| Company, Individual | [optional]
 **zip** | **string**|  | [optional]
 **city** | **string**|  | [optional]
 **state** | **string**|  | [optional]
 **country** | **string**|  | [optional]
 **phone** | **string**|  | [optional]
 **not_valid** | **bool**|  | [optional]
 **page_index** | **int**|  | [optional]
 **page_size** | **int**|  | [optional]
 **sort_info** | **string**|  | [optional]

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customerPost**
> object customerPost($body)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new PlanningPme\Api\CustomerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \PlanningPme\Model\PlanningPMEAPIModelsCustomerModel(); // \PlanningPme\Model\PlanningPMEAPIModelsCustomerModel | 

try {
    $result = $apiInstance->customerPost($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerApi->customerPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\PlanningPme\Model\PlanningPMEAPIModelsCustomerModel**](../Model/PlanningPMEAPIModelsCustomerModel.md)|  |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customerPut**
> object customerPut($body, $id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new PlanningPme\Api\CustomerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \PlanningPme\Model\PlanningPMEAPIModelsCustomerModel(); // \PlanningPme\Model\PlanningPMEAPIModelsCustomerModel | 
$id = 56; // int | 

try {
    $result = $apiInstance->customerPut($body, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerApi->customerPut: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\PlanningPme\Model\PlanningPMEAPIModelsCustomerModel**](../Model/PlanningPMEAPIModelsCustomerModel.md)|  |
 **id** | **int**|  |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

