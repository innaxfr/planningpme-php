# PlanningPme\ProjectApi

All URIs are relative to *https://api.planningpme.com/innax*

Method | HTTP request | Description
------------- | ------------- | -------------
[**projectArrange**](ProjectApi.md#projectarrange) | **POST** /api/project/{id}/arrange | 
[**projectDelete**](ProjectApi.md#projectdelete) | **DELETE** /api/project | 
[**projectGet**](ProjectApi.md#projectget) | **GET** /api/project | 
[**projectGet_0**](ProjectApi.md#projectget_0) | **GET** /api/project/{id} | 
[**projectPost**](ProjectApi.md#projectpost) | **POST** /api/project | 
[**projectPut**](ProjectApi.md#projectput) | **PUT** /api/project/{id} | 

# **projectArrange**
> object projectArrange($body, $id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new PlanningPme\Api\ProjectApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \PlanningPme\Model\PlanningPMEAPIModelsProjectArrangeModel(); // \PlanningPme\Model\PlanningPMEAPIModelsProjectArrangeModel | 
$id = 56; // int | 

try {
    $result = $apiInstance->projectArrange($body, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProjectApi->projectArrange: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\PlanningPme\Model\PlanningPMEAPIModelsProjectArrangeModel**](../Model/PlanningPMEAPIModelsProjectArrangeModel.md)|  |
 **id** | **int**|  |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **projectDelete**
> object projectDelete($body)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new PlanningPme\Api\ProjectApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = array(56); // int[] | 

try {
    $result = $apiInstance->projectDelete($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProjectApi->projectDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**int[]**](../Model/int.md)|  |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **projectGet**
> object projectGet($name, $not_valid, $customers, $departments, $access, $page_index, $page_size, $sort_info, $json_writing_type, $with_fields, $sub_project_estimate)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new PlanningPme\Api\ProjectApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$name = "name_example"; // string | 
$not_valid = true; // bool | 
$customers = "customers_example"; // string | 
$departments = "departments_example"; // string | 
$access = "access_example"; // string | All, Read, Write
$page_index = 56; // int | 
$page_size = 56; // int | 
$sort_info = "sort_info_example"; // string | 
$json_writing_type = "json_writing_type_example"; // string | None, Normal, KeyLabel, String, Data
$with_fields = true; // bool | 
$sub_project_estimate = true; // bool | 

try {
    $result = $apiInstance->projectGet($name, $not_valid, $customers, $departments, $access, $page_index, $page_size, $sort_info, $json_writing_type, $with_fields, $sub_project_estimate);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProjectApi->projectGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **string**|  | [optional]
 **not_valid** | **bool**|  | [optional]
 **customers** | **string**|  | [optional]
 **departments** | **string**|  | [optional]
 **access** | **string**| All, Read, Write | [optional]
 **page_index** | **int**|  | [optional]
 **page_size** | **int**|  | [optional]
 **sort_info** | **string**|  | [optional]
 **json_writing_type** | **string**| None, Normal, KeyLabel, String, Data | [optional]
 **with_fields** | **bool**|  | [optional]
 **sub_project_estimate** | **bool**|  | [optional]

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **projectGet_0**
> object projectGet_0($id, $with_sub_projects)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new PlanningPme\Api\ProjectApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | 
$with_sub_projects = true; // bool | 

try {
    $result = $apiInstance->projectGet_0($id, $with_sub_projects);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProjectApi->projectGet_0: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |
 **with_sub_projects** | **bool**|  | [optional]

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **projectPost**
> object projectPost($body)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new PlanningPme\Api\ProjectApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \PlanningPme\Model\PlanningPMEAPIModelsProjectModel(); // \PlanningPme\Model\PlanningPMEAPIModelsProjectModel | 

try {
    $result = $apiInstance->projectPost($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProjectApi->projectPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\PlanningPme\Model\PlanningPMEAPIModelsProjectModel**](../Model/PlanningPMEAPIModelsProjectModel.md)|  |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **projectPut**
> object projectPut($body, $id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new PlanningPme\Api\ProjectApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \PlanningPme\Model\PlanningPMEAPIModelsProjectModel(); // \PlanningPme\Model\PlanningPMEAPIModelsProjectModel | 
$id = 56; // int | 

try {
    $result = $apiInstance->projectPut($body, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProjectApi->projectPut: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\PlanningPme\Model\PlanningPMEAPIModelsProjectModel**](../Model/PlanningPMEAPIModelsProjectModel.md)|  |
 **id** | **int**|  |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

