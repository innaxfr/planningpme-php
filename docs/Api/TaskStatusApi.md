# PlanningPme\TaskStatusApi

All URIs are relative to *https://api.planningpme.com/innax*

Method | HTTP request | Description
------------- | ------------- | -------------
[**taskStatusDelete**](TaskStatusApi.md#taskstatusdelete) | **DELETE** /api/taskstatus | 
[**taskStatusGet**](TaskStatusApi.md#taskstatusget) | **GET** /api/taskstatus | 
[**taskStatusGet_0**](TaskStatusApi.md#taskstatusget_0) | **GET** /api/taskstatus/{id} | 
[**taskStatusPost**](TaskStatusApi.md#taskstatuspost) | **POST** /api/taskstatus | 
[**taskStatusPut**](TaskStatusApi.md#taskstatusput) | **PUT** /api/taskstatus/{id} | 

# **taskStatusDelete**
> object taskStatusDelete($body)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new PlanningPme\Api\TaskStatusApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = array(56); // int[] | 

try {
    $result = $apiInstance->taskStatusDelete($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TaskStatusApi->taskStatusDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**int[]**](../Model/int.md)|  |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **taskStatusGet**
> object taskStatusGet($label, $page_index, $page_size, $sort_info)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new PlanningPme\Api\TaskStatusApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$label = "label_example"; // string | 
$page_index = 56; // int | 
$page_size = 56; // int | 
$sort_info = "sort_info_example"; // string | 

try {
    $result = $apiInstance->taskStatusGet($label, $page_index, $page_size, $sort_info);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TaskStatusApi->taskStatusGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **label** | **string**|  | [optional]
 **page_index** | **int**|  | [optional]
 **page_size** | **int**|  | [optional]
 **sort_info** | **string**|  | [optional]

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **taskStatusGet_0**
> object taskStatusGet_0($id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new PlanningPme\Api\TaskStatusApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | 

try {
    $result = $apiInstance->taskStatusGet_0($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TaskStatusApi->taskStatusGet_0: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **taskStatusPost**
> object taskStatusPost($body)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new PlanningPme\Api\TaskStatusApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \PlanningPme\Model\PlanningPMEAPIModelsTaskStatusModel(); // \PlanningPme\Model\PlanningPMEAPIModelsTaskStatusModel | 

try {
    $result = $apiInstance->taskStatusPost($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TaskStatusApi->taskStatusPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\PlanningPme\Model\PlanningPMEAPIModelsTaskStatusModel**](../Model/PlanningPMEAPIModelsTaskStatusModel.md)|  |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **taskStatusPut**
> object taskStatusPut($body, $id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new PlanningPme\Api\TaskStatusApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \PlanningPme\Model\PlanningPMEAPIModelsTaskStatusModel(); // \PlanningPme\Model\PlanningPMEAPIModelsTaskStatusModel | 
$id = 56; // int | 

try {
    $result = $apiInstance->taskStatusPut($body, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TaskStatusApi->taskStatusPut: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\PlanningPme\Model\PlanningPMEAPIModelsTaskStatusModel**](../Model/PlanningPMEAPIModelsTaskStatusModel.md)|  |
 **id** | **int**|  |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

