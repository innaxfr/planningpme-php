# PlanningPme\ExportApi

All URIs are relative to *https://api.planningpme.com/innax*

Method | HTTP request | Description
------------- | ------------- | -------------
[**exportGetAuth**](ExportApi.md#exportgetauth) | **POST** /api/export | 
[**exportGetJson**](ExportApi.md#exportgetjson) | **POST** /api/export/json | 
[**exportGetStream**](ExportApi.md#exportgetstream) | **GET** /api/export/{authId}/stream | 

# **exportGetAuth**
> object exportGetAuth($body)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new PlanningPme\Api\ExportApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \PlanningPme\Model\PlanningPMEAPIModelsExportModelWithTemplate(); // \PlanningPme\Model\PlanningPMEAPIModelsExportModelWithTemplate | 

try {
    $result = $apiInstance->exportGetAuth($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ExportApi->exportGetAuth: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\PlanningPme\Model\PlanningPMEAPIModelsExportModelWithTemplate**](../Model/PlanningPMEAPIModelsExportModelWithTemplate.md)|  |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **exportGetJson**
> object exportGetJson($body)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new PlanningPme\Api\ExportApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \PlanningPme\Model\PlanningPMEAPIModelsExportModel(); // \PlanningPme\Model\PlanningPMEAPIModelsExportModel | 

try {
    $result = $apiInstance->exportGetJson($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ExportApi->exportGetJson: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\PlanningPme\Model\PlanningPMEAPIModelsExportModel**](../Model/PlanningPMEAPIModelsExportModel.md)|  |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **exportGetStream**
> object exportGetStream($auth_id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new PlanningPme\Api\ExportApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$auth_id = "auth_id_example"; // string | 

try {
    $result = $apiInstance->exportGetStream($auth_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ExportApi->exportGetStream: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **auth_id** | **string**|  |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

