# PlanningPme\SubProjectApi

All URIs are relative to *https://api.planningpme.com/innax*

Method | HTTP request | Description
------------- | ------------- | -------------
[**subProjectDelete**](SubProjectApi.md#subprojectdelete) | **DELETE** /api/subproject | 
[**subProjectGet**](SubProjectApi.md#subprojectget) | **GET** /api/subproject | 
[**subProjectGet_0**](SubProjectApi.md#subprojectget_0) | **GET** /api/subproject/{id} | 
[**subProjectPost**](SubProjectApi.md#subprojectpost) | **POST** /api/subproject | 
[**subProjectPut**](SubProjectApi.md#subprojectput) | **PUT** /api/subproject/{id} | 

# **subProjectDelete**
> object subProjectDelete($body)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new PlanningPme\Api\SubProjectApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = array(56); // int[] | 

try {
    $result = $apiInstance->subProjectDelete($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SubProjectApi->subProjectDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**int[]**](../Model/int.md)|  |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **subProjectGet**
> object subProjectGet($project_key, $name, $page_index, $page_size, $sort_info, $json_writing_type, $with_fields)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new PlanningPme\Api\SubProjectApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$project_key = 56; // int | 
$name = "name_example"; // string | 
$page_index = 56; // int | 
$page_size = 56; // int | 
$sort_info = "sort_info_example"; // string | 
$json_writing_type = "json_writing_type_example"; // string | None, Normal, KeyLabel, String, Data
$with_fields = true; // bool | 

try {
    $result = $apiInstance->subProjectGet($project_key, $name, $page_index, $page_size, $sort_info, $json_writing_type, $with_fields);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SubProjectApi->subProjectGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **project_key** | **int**|  |
 **name** | **string**|  | [optional]
 **page_index** | **int**|  | [optional]
 **page_size** | **int**|  | [optional]
 **sort_info** | **string**|  | [optional]
 **json_writing_type** | **string**| None, Normal, KeyLabel, String, Data | [optional]
 **with_fields** | **bool**|  | [optional]

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **subProjectGet_0**
> object subProjectGet_0($id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new PlanningPme\Api\SubProjectApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | 

try {
    $result = $apiInstance->subProjectGet_0($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SubProjectApi->subProjectGet_0: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **subProjectPost**
> object subProjectPost($body)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new PlanningPme\Api\SubProjectApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \PlanningPme\Model\PlanningPMEAPIModelsSubProjectModel(); // \PlanningPme\Model\PlanningPMEAPIModelsSubProjectModel | 

try {
    $result = $apiInstance->subProjectPost($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SubProjectApi->subProjectPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\PlanningPme\Model\PlanningPMEAPIModelsSubProjectModel**](../Model/PlanningPMEAPIModelsSubProjectModel.md)|  |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **subProjectPut**
> object subProjectPut($body, $id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new PlanningPme\Api\SubProjectApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \PlanningPme\Model\PlanningPMEAPIModelsSubProjectModel(); // \PlanningPme\Model\PlanningPMEAPIModelsSubProjectModel | 
$id = 56; // int | 

try {
    $result = $apiInstance->subProjectPut($body, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SubProjectApi->subProjectPut: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\PlanningPme\Model\PlanningPMEAPIModelsSubProjectModel**](../Model/PlanningPMEAPIModelsSubProjectModel.md)|  |
 **id** | **int**|  |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

