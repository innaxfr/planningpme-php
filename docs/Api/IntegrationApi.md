# PlanningPme\IntegrationApi

All URIs are relative to *https://api.planningpme.com/innax*

Method | HTTP request | Description
------------- | ------------- | -------------
[**integrationDeleteTemplate**](IntegrationApi.md#integrationdeletetemplate) | **DELETE** /api/integration/template | 
[**integrationGet**](IntegrationApi.md#integrationget) | **GET** /api/integration | 
[**integrationGetSchema**](IntegrationApi.md#integrationgetschema) | **GET** /api/integration/schema | 
[**integrationGetTemplates**](IntegrationApi.md#integrationgettemplates) | **GET** /api/integration/templates | 
[**integrationPost**](IntegrationApi.md#integrationpost) | **POST** /api/integration | 
[**integrationPostTemplate**](IntegrationApi.md#integrationposttemplate) | **POST** /api/integration/template | 
[**integrationPutTemplate**](IntegrationApi.md#integrationputtemplate) | **PUT** /api/integration/template | 

# **integrationDeleteTemplate**
> object integrationDeleteTemplate($name)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new PlanningPme\Api\IntegrationApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$name = "name_example"; // string | 

try {
    $result = $apiInstance->integrationDeleteTemplate($name);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling IntegrationApi->integrationDeleteTemplate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **string**|  |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **integrationGet**
> object integrationGet($name, $since)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new PlanningPme\Api\IntegrationApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$name = "name_example"; // string | 
$since = "since_example"; // string | 

try {
    $result = $apiInstance->integrationGet($name, $since);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling IntegrationApi->integrationGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **string**|  |
 **since** | **string**|  | [optional]

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **integrationGetSchema**
> object integrationGetSchema()



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new PlanningPme\Api\IntegrationApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->integrationGetSchema();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling IntegrationApi->integrationGetSchema: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **integrationGetTemplates**
> object integrationGetTemplates()



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new PlanningPme\Api\IntegrationApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->integrationGetTemplates();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling IntegrationApi->integrationGetTemplates: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **integrationPost**
> object integrationPost($body, $name, $simulate, $verbose, $stop_on_error)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new PlanningPme\Api\IntegrationApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \stdClass; // object | 
$name = "name_example"; // string | 
$simulate = true; // bool | 
$verbose = true; // bool | 
$stop_on_error = true; // bool | 

try {
    $result = $apiInstance->integrationPost($body, $name, $simulate, $verbose, $stop_on_error);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling IntegrationApi->integrationPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**object**](../Model/object.md)|  |
 **name** | **string**|  |
 **simulate** | **bool**|  |
 **verbose** | **bool**|  |
 **stop_on_error** | **bool**|  |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **integrationPostTemplate**
> object integrationPostTemplate($body)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new PlanningPme\Api\IntegrationApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \PlanningPme\Model\PlanningPMECoreIntegrationXmlConfig(); // \PlanningPme\Model\PlanningPMECoreIntegrationXmlConfig | 

try {
    $result = $apiInstance->integrationPostTemplate($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling IntegrationApi->integrationPostTemplate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\PlanningPme\Model\PlanningPMECoreIntegrationXmlConfig**](../Model/PlanningPMECoreIntegrationXmlConfig.md)|  |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **integrationPutTemplate**
> object integrationPutTemplate($body, $name)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new PlanningPme\Api\IntegrationApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \PlanningPme\Model\PlanningPMECoreIntegrationXmlConfig(); // \PlanningPme\Model\PlanningPMECoreIntegrationXmlConfig | 
$name = "name_example"; // string | 

try {
    $result = $apiInstance->integrationPutTemplate($body, $name);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling IntegrationApi->integrationPutTemplate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\PlanningPme\Model\PlanningPMECoreIntegrationXmlConfig**](../Model/PlanningPMECoreIntegrationXmlConfig.md)|  |
 **name** | **string**|  |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

