# PlanningPme\UnavailabilityStatusApi

All URIs are relative to *https://api.planningpme.com/innax*

Method | HTTP request | Description
------------- | ------------- | -------------
[**unavailabilityStatusDelete**](UnavailabilityStatusApi.md#unavailabilitystatusdelete) | **DELETE** /api/unavailabilitystatus | 
[**unavailabilityStatusGet**](UnavailabilityStatusApi.md#unavailabilitystatusget) | **GET** /api/unavailabilitystatus | 
[**unavailabilityStatusGet_0**](UnavailabilityStatusApi.md#unavailabilitystatusget_0) | **GET** /api/unavailabilitystatus/{id} | 
[**unavailabilityStatusPost**](UnavailabilityStatusApi.md#unavailabilitystatuspost) | **POST** /api/unavailabilitystatus | 
[**unavailabilityStatusPut**](UnavailabilityStatusApi.md#unavailabilitystatusput) | **PUT** /api/unavailabilitystatus/{id} | 

# **unavailabilityStatusDelete**
> object unavailabilityStatusDelete($body)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new PlanningPme\Api\UnavailabilityStatusApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = array(56); // int[] | 

try {
    $result = $apiInstance->unavailabilityStatusDelete($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UnavailabilityStatusApi->unavailabilityStatusDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**int[]**](../Model/int.md)|  |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **unavailabilityStatusGet**
> object unavailabilityStatusGet($label, $page_index, $page_size, $sort_info)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new PlanningPme\Api\UnavailabilityStatusApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$label = "label_example"; // string | 
$page_index = 56; // int | 
$page_size = 56; // int | 
$sort_info = "sort_info_example"; // string | 

try {
    $result = $apiInstance->unavailabilityStatusGet($label, $page_index, $page_size, $sort_info);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UnavailabilityStatusApi->unavailabilityStatusGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **label** | **string**|  | [optional]
 **page_index** | **int**|  | [optional]
 **page_size** | **int**|  | [optional]
 **sort_info** | **string**|  | [optional]

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **unavailabilityStatusGet_0**
> object unavailabilityStatusGet_0($id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new PlanningPme\Api\UnavailabilityStatusApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | 

try {
    $result = $apiInstance->unavailabilityStatusGet_0($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UnavailabilityStatusApi->unavailabilityStatusGet_0: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **unavailabilityStatusPost**
> object unavailabilityStatusPost($body)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new PlanningPme\Api\UnavailabilityStatusApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \PlanningPme\Model\PlanningPMEAPIModelsUnavailabilityStatusModel(); // \PlanningPme\Model\PlanningPMEAPIModelsUnavailabilityStatusModel | 

try {
    $result = $apiInstance->unavailabilityStatusPost($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UnavailabilityStatusApi->unavailabilityStatusPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\PlanningPme\Model\PlanningPMEAPIModelsUnavailabilityStatusModel**](../Model/PlanningPMEAPIModelsUnavailabilityStatusModel.md)|  |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **unavailabilityStatusPut**
> object unavailabilityStatusPut($body, $id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new PlanningPme\Api\UnavailabilityStatusApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \PlanningPme\Model\PlanningPMEAPIModelsUnavailabilityStatusModel(); // \PlanningPme\Model\PlanningPMEAPIModelsUnavailabilityStatusModel | 
$id = 56; // int | 

try {
    $result = $apiInstance->unavailabilityStatusPut($body, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UnavailabilityStatusApi->unavailabilityStatusPut: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\PlanningPme\Model\PlanningPMEAPIModelsUnavailabilityStatusModel**](../Model/PlanningPMEAPIModelsUnavailabilityStatusModel.md)|  |
 **id** | **int**|  |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

