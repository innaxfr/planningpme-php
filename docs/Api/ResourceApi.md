# PlanningPme\ResourceApi

All URIs are relative to *https://api.planningpme.com/innax*

Method | HTTP request | Description
------------- | ------------- | -------------
[**resourceDelete**](ResourceApi.md#resourcedelete) | **DELETE** /api/resource | 
[**resourceGet**](ResourceApi.md#resourceget) | **GET** /api/resource | 
[**resourceGet_0**](ResourceApi.md#resourceget_0) | **GET** /api/resource/{id} | 
[**resourcePost**](ResourceApi.md#resourcepost) | **POST** /api/resource | 
[**resourcePut**](ResourceApi.md#resourceput) | **PUT** /api/resource/{id} | 

# **resourceDelete**
> object resourceDelete($body)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new PlanningPme\Api\ResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = array(56); // int[] | 

try {
    $result = $apiInstance->resourceDelete($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ResourceApi->resourceDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**int[]**](../Model/int.md)|  |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **resourceGet**
> object resourceGet($name, $filters, $type, $skills, $not_valid, $fix, $access, $available, $from, $to, $except_do_key, $page_index, $page_size, $sort_info, $json_writing_type, $filters_junction, $planning_sort_rows_by)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new PlanningPme\Api\ResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$name = "name_example"; // string | 
$filters = "filters_example"; // string | 
$type = "type_example"; // string | Human, Material, ToPlan, Extern
$skills = "skills_example"; // string | 
$not_valid = true; // bool | 
$fix = true; // bool | 
$access = "access_example"; // string | All, Read, Write
$available = true; // bool | 
$from = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | 
$to = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | 
$except_do_key = 56; // int | 
$page_index = 56; // int | 
$page_size = 56; // int | 
$sort_info = "sort_info_example"; // string | 
$json_writing_type = "json_writing_type_example"; // string | None, Normal, KeyLabel, String, Data
$filters_junction = true; // bool | 
$planning_sort_rows_by = "planning_sort_rows_by_example"; // string | Resource, Department

try {
    $result = $apiInstance->resourceGet($name, $filters, $type, $skills, $not_valid, $fix, $access, $available, $from, $to, $except_do_key, $page_index, $page_size, $sort_info, $json_writing_type, $filters_junction, $planning_sort_rows_by);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ResourceApi->resourceGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **string**|  | [optional]
 **filters** | **string**|  | [optional]
 **type** | **string**| Human, Material, ToPlan, Extern | [optional]
 **skills** | **string**|  | [optional]
 **not_valid** | **bool**|  | [optional]
 **fix** | **bool**|  | [optional]
 **access** | **string**| All, Read, Write | [optional]
 **available** | **bool**|  | [optional]
 **from** | **\DateTime**|  | [optional]
 **to** | **\DateTime**|  | [optional]
 **except_do_key** | **int**|  | [optional]
 **page_index** | **int**|  | [optional]
 **page_size** | **int**|  | [optional]
 **sort_info** | **string**|  | [optional]
 **json_writing_type** | **string**| None, Normal, KeyLabel, String, Data | [optional]
 **filters_junction** | **bool**|  | [optional]
 **planning_sort_rows_by** | **string**| Resource, Department | [optional]

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **resourceGet_0**
> object resourceGet_0($id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new PlanningPme\Api\ResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | 

try {
    $result = $apiInstance->resourceGet_0($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ResourceApi->resourceGet_0: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **resourcePost**
> object resourcePost($body)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new PlanningPme\Api\ResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \PlanningPme\Model\PlanningPMEAPIModelsResourceModel(); // \PlanningPme\Model\PlanningPMEAPIModelsResourceModel | 

try {
    $result = $apiInstance->resourcePost($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ResourceApi->resourcePost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\PlanningPme\Model\PlanningPMEAPIModelsResourceModel**](../Model/PlanningPMEAPIModelsResourceModel.md)|  |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **resourcePut**
> object resourcePut($body, $id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new PlanningPme\Api\ResourceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \PlanningPme\Model\PlanningPMEAPIModelsResourceModel(); // \PlanningPme\Model\PlanningPMEAPIModelsResourceModel | 
$id = 56; // int | 

try {
    $result = $apiInstance->resourcePut($body, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ResourceApi->resourcePut: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\PlanningPme\Model\PlanningPMEAPIModelsResourceModel**](../Model/PlanningPMEAPIModelsResourceModel.md)|  |
 **id** | **int**|  |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

