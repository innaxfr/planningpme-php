# PlanningPme\HistoryApi

All URIs are relative to *https://api.planningpme.com/innax*

Method | HTTP request | Description
------------- | ------------- | -------------
[**historyGet**](HistoryApi.md#historyget) | **GET** /api/history | 

# **historyGet**
> object historyGet($name, $key, $type, $operation, $since, $page_index, $page_size, $sort_info, $exclude_users)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new PlanningPme\Api\HistoryApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$name = "name_example"; // string | 
$key = 56; // int | 
$type = "type_example"; // string | Task, Unavailability, Resource, Customer, Project, SessionDesktop, SessionWebAccess, SessionMobile, Assignment
$operation = "operation_example"; // string | Insert, Delete, Update, Email, Invitation, Session, Unperiodize
$since = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | 
$page_index = 56; // int | 
$page_size = 56; // int | 
$sort_info = "sort_info_example"; // string | 
$exclude_users = "exclude_users_example"; // string | 

try {
    $result = $apiInstance->historyGet($name, $key, $type, $operation, $since, $page_index, $page_size, $sort_info, $exclude_users);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling HistoryApi->historyGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **string**|  | [optional]
 **key** | **int**|  | [optional]
 **type** | **string**| Task, Unavailability, Resource, Customer, Project, SessionDesktop, SessionWebAccess, SessionMobile, Assignment | [optional]
 **operation** | **string**| Insert, Delete, Update, Email, Invitation, Session, Unperiodize | [optional]
 **since** | **\DateTime**|  | [optional]
 **page_index** | **int**|  | [optional]
 **page_size** | **int**|  | [optional]
 **sort_info** | **string**|  | [optional]
 **exclude_users** | **string**|  | [optional]

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

