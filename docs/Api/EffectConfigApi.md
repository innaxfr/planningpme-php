# PlanningPme\EffectConfigApi

All URIs are relative to *https://api.planningpme.com/innax*

Method | HTTP request | Description
------------- | ------------- | -------------
[**effectConfigDelete**](EffectConfigApi.md#effectconfigdelete) | **DELETE** /api/effectconfig | 
[**effectConfigGet**](EffectConfigApi.md#effectconfigget) | **GET** /api/effectconfig | 
[**effectConfigGet_0**](EffectConfigApi.md#effectconfigget_0) | **GET** /api/effectconfig/{id} | 
[**effectConfigPost**](EffectConfigApi.md#effectconfigpost) | **POST** /api/effectconfig | 
[**effectConfigPut**](EffectConfigApi.md#effectconfigput) | **PUT** /api/effectconfig/{id} | 

# **effectConfigDelete**
> object effectConfigDelete($body)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new PlanningPme\Api\EffectConfigApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = array(56); // int[] | 

try {
    $result = $apiInstance->effectConfigDelete($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling EffectConfigApi->effectConfigDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**int[]**](../Model/int.md)|  |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **effectConfigGet**
> object effectConfigGet($label, $subject_type, $enabled, $page_index, $page_size, $sort_info)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new PlanningPme\Api\EffectConfigApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$label = "label_example"; // string | 
$subject_type = "subject_type_example"; // string | Assignment, Task, Unavailability
$enabled = true; // bool | 
$page_index = 56; // int | 
$page_size = 56; // int | 
$sort_info = "sort_info_example"; // string | 

try {
    $result = $apiInstance->effectConfigGet($label, $subject_type, $enabled, $page_index, $page_size, $sort_info);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling EffectConfigApi->effectConfigGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **label** | **string**|  | [optional]
 **subject_type** | **string**| Assignment, Task, Unavailability | [optional]
 **enabled** | **bool**|  | [optional]
 **page_index** | **int**|  | [optional]
 **page_size** | **int**|  | [optional]
 **sort_info** | **string**|  | [optional]

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **effectConfigGet_0**
> object effectConfigGet_0($id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new PlanningPme\Api\EffectConfigApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | 

try {
    $result = $apiInstance->effectConfigGet_0($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling EffectConfigApi->effectConfigGet_0: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **effectConfigPost**
> object effectConfigPost($body)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new PlanningPme\Api\EffectConfigApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \PlanningPme\Model\PlanningPMEAPIModelsEffDefinitionModel(); // \PlanningPme\Model\PlanningPMEAPIModelsEffDefinitionModel | 

try {
    $result = $apiInstance->effectConfigPost($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling EffectConfigApi->effectConfigPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\PlanningPme\Model\PlanningPMEAPIModelsEffDefinitionModel**](../Model/PlanningPMEAPIModelsEffDefinitionModel.md)|  |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **effectConfigPut**
> object effectConfigPut($body, $id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new PlanningPme\Api\EffectConfigApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \PlanningPme\Model\PlanningPMEAPIModelsEffDefinitionModel(); // \PlanningPme\Model\PlanningPMEAPIModelsEffDefinitionModel | 
$id = 56; // int | 

try {
    $result = $apiInstance->effectConfigPut($body, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling EffectConfigApi->effectConfigPut: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\PlanningPme\Model\PlanningPMEAPIModelsEffDefinitionModel**](../Model/PlanningPMEAPIModelsEffDefinitionModel.md)|  |
 **id** | **int**|  |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

