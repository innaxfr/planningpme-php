# PlanningPme\ContractApi

All URIs are relative to *https://api.planningpme.com/innax*

Method | HTTP request | Description
------------- | ------------- | -------------
[**contractDelete**](ContractApi.md#contractdelete) | **DELETE** /api/contract | 
[**contractGet**](ContractApi.md#contractget) | **GET** /api/contract/{id} | 
[**contractPost**](ContractApi.md#contractpost) | **POST** /api/contract | 
[**contractPut**](ContractApi.md#contractput) | **PUT** /api/contract/{id} | 

# **contractDelete**
> object contractDelete($body)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new PlanningPme\Api\ContractApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = array(56); // int[] | 

try {
    $result = $apiInstance->contractDelete($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ContractApi->contractDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**int[]**](../Model/int.md)|  |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **contractGet**
> object contractGet($id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new PlanningPme\Api\ContractApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | 

try {
    $result = $apiInstance->contractGet($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ContractApi->contractGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **contractPost**
> object contractPost($body)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new PlanningPme\Api\ContractApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \PlanningPme\Model\PlanningPMEAPIModelsContractModel(); // \PlanningPme\Model\PlanningPMEAPIModelsContractModel | 

try {
    $result = $apiInstance->contractPost($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ContractApi->contractPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\PlanningPme\Model\PlanningPMEAPIModelsContractModel**](../Model/PlanningPMEAPIModelsContractModel.md)|  |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **contractPut**
> object contractPut($body, $id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new PlanningPme\Api\ContractApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \PlanningPme\Model\PlanningPMEAPIModelsContractModel(); // \PlanningPme\Model\PlanningPMEAPIModelsContractModel | 
$id = 56; // int | 

try {
    $result = $apiInstance->contractPut($body, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ContractApi->contractPut: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\PlanningPme\Model\PlanningPMEAPIModelsContractModel**](../Model/PlanningPMEAPIModelsContractModel.md)|  |
 **id** | **int**|  |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

