# PlanningPme\ForgotApi

All URIs are relative to *https://api.planningpme.com/innax*

Method | HTTP request | Description
------------- | ------------- | -------------
[**forgotPost**](ForgotApi.md#forgotpost) | **POST** /api/forgot | 

# **forgotPost**
> object forgotPost($body)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new PlanningPme\Api\ForgotApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \PlanningPme\Model\PlanningPMEAPIModelsForgotModel(); // \PlanningPme\Model\PlanningPMEAPIModelsForgotModel | 

try {
    $result = $apiInstance->forgotPost($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ForgotApi->forgotPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\PlanningPme\Model\PlanningPMEAPIModelsForgotModel**](../Model/PlanningPMEAPIModelsForgotModel.md)|  |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

